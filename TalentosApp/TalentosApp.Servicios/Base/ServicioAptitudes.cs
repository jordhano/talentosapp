﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contractos.AccesoDatos.Repositorios;
using TalentosApp.Contractos.Servicios;
using TalentosApp.Contratos.AccesoDatos;
using TalentosApp.Enumeradores;
using TalentosApp.Nucleo.Entidades;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.TransferenciaDatos.VistaModelos;

namespace TalentosApp.Servicios.Base
{
    public class ServicioAptitudes: IServicioAptitudes
    {
        private readonly IUnidadTrabajo _unidadTrabajo;
        public ServicioAptitudes(IUnidadTrabajo unidadTrabajo)
        {
            _unidadTrabajo = unidadTrabajo;
        }

        public IEnumerable<AptitudesVw> ObtenerAptitudes()
        {
            var _repositorioAptitudes = _unidadTrabajo.RepositorioAptitudes();
            var aptitudes = _repositorioAptitudes.ObtenerRegistros(a => a.EstadoReg == true);
            
            var existeResultados = aptitudes != null && aptitudes.Any();
            if (!existeResultados)
                return Enumerable.Empty<AptitudesVw>();

            return aptitudes.Select(a => new AptitudesVw()
            {
                AptitudID = a.AptitudID,
                Nombre = a.Nombre,
                Descripcion = a.Descripcion,
                TipoAptitud = a.TipoAptitud
            });
        }

        public int RegistrarAptitud(AptitudInputModel entrada)
        {
            var esNombreValido = !string.IsNullOrEmpty(entrada.Nombre);
            if (!esNombreValido)
                return (int)ErroresComunes.CampoVacio;
            
            var existeTipoAptitud = Enum.IsDefined(typeof(TipoAptitud), entrada.TipoAptitud);
            if (!existeTipoAptitud)
                return (int)ErroresComunes.CampoInvalidado;

            var aptitudDB = new Aptitudes()
            {
                Nombre = entrada.Nombre,
                Descripcion = entrada.Descripcion,
                TipoAptitud = entrada.TipoAptitud,
                GuidReg = Guid.NewGuid(),
                EstadoReg = true,
                FechaModificacion = DateTime.Now
            };

            var _repositorioAptitudes = _unidadTrabajo.RepositorioAptitudes();
            _repositorioAptitudes.Insertar(aptitudDB);
            var cambios = _unidadTrabajo.GuardarCambios();
            if (cambios)
                return aptitudDB.AptitudID;
            else
                return (int) ErroresComunes.ErrorGuardando;
        }


        public IEnumerable<AptitudesVw> ObtenerAptitudesDisponiblePerfil(int perfilID)
        {
            var _repositorioAptitudes = _unidadTrabajo.RepositorioAptitudes();
            var aptitudes = _repositorioAptitudes.ObtenerAptitudesDisponiblesPerfil<AptitudesVw>(perfilID);
            var existenResultados = aptitudes != null && aptitudes.Any();
            
            if (!existenResultados)
                return Enumerable.Empty<AptitudesVw>();

            return aptitudes;
        }

        public IEnumerable<AptitudesVw> ObtenerAptitudesDisponibleTalentos(int talentoID)
        {
            var _repositorioAptitudes = _unidadTrabajo.RepositorioAptitudes();
            var aptitudes = _repositorioAptitudes.ObtenerAptitudesDisponiblesTalento<AptitudesVw>(talentoID);

            var existenResultados = aptitudes != null && aptitudes.Any();
            if (!existenResultados)
                return Enumerable.Empty<AptitudesVw>();

            return aptitudes;
        }

        public IEnumerable<AptitudesVw> ObtenerAptitudesDisponibleVacantes(int vacanteID)
        {
            var _repositorioAptitudes = _unidadTrabajo.RepositorioAptitudes();
            var aptitudes = _repositorioAptitudes.ObtenerAptitudesDisponiblesVacante<AptitudesVw>(vacanteID);
            var existenResultados = aptitudes != null && aptitudes.Any();

            if (!existenResultados)
                return Enumerable.Empty<AptitudesVw>();

            return aptitudes;
        }


        public AptitudesVw ObtenerAptitud(int id)
        {
            var _repositorioAptitudes = _unidadTrabajo.RepositorioAptitudes();
            var aptitud = _repositorioAptitudes.ObtenerRegistro(a => a.EstadoReg == true && a.AptitudID == id);
            if (aptitud == null)
                return null;

            return new AptitudesVw()
            {
                AptitudID = aptitud.AptitudID,
                Nombre = aptitud.Nombre,
                Descripcion = aptitud.Descripcion,
                TipoAptitud = aptitud.TipoAptitud
            };
        }


        public int ModificarAptitud(AptitudInputModel entrada)
        {
            var _repositorioAptitudes = _unidadTrabajo.RepositorioAptitudes();
            var aptitudDB = _repositorioAptitudes.ObtenerRegistro(a => a.EstadoReg == true && a.AptitudID == entrada.AptitudID);

            var existeAptitud = aptitudDB == null;
            if (!existeAptitud)
                return (int)ErroresComunes.ValoresNoEncontrado;

            var esNombreValido = !string.IsNullOrEmpty(entrada.Nombre);
            if (esNombreValido)
                return (int)ErroresComunes.CampoVacio;

            var existeTipoAptitud = Enum.IsDefined(typeof(TipoAptitud), entrada.TipoAptitud);
            if (existeTipoAptitud)
                return (int)ErroresComunes.CampoInvalidado;

            aptitudDB.Nombre = entrada.Nombre;
            aptitudDB.Descripcion = entrada.Descripcion;
            aptitudDB.TipoAptitud = entrada.TipoAptitud;
            aptitudDB.FechaModificacion = DateTime.Now;

            var cambios = _unidadTrabajo.GuardarCambios();
            if (cambios)
                return aptitudDB.AptitudID;
            else
                return (int)ErroresComunes.ErrorGuardando;
        }


        public int RemoverAptitud(int aptitudId)
        {
            var _repositorioAptitudes = _unidadTrabajo.RepositorioAptitudes();
            var aptitudDB = _repositorioAptitudes.ObtenerRegistro(a => a.EstadoReg == true && a.AptitudID == aptitudId);
            
            var existeAptitud = aptitudDB == null;
            if (!existeAptitud)
                return (int)ErroresComunes.ValoresNoEncontrado;

            aptitudDB.EstadoReg = false;
            aptitudDB.PerfilesAptitudes.ToList().ForEach(pa => pa.EstadoReg = false);
            aptitudDB.TalentosAptitudes.ToList().ForEach(pa => pa.EstadoReg = false);
            aptitudDB.VacantesAptitudes.ToList().ForEach(pa => pa.EstadoReg = false);
            aptitudDB.VacantePriorizacion.ToList().ForEach(va => va.EstadoReg = false);

            var cambios = _unidadTrabajo.GuardarCambios();
            if (cambios)
                return aptitudDB.AptitudID;
            else
                return (int)ErroresComunes.ErrorGuardando;
        }
    }
}
