﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contractos.AccesoDatos.Repositorios;
using TalentosApp.Contractos.Servicios;
using TalentosApp.Contratos.AccesoDatos;
using TalentosApp.Contratos.AccesoDatos.Repositorios;
using TalentosApp.Enumeradores;
using TalentosApp.Nucleo.Entidades;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.TransferenciaDatos.VistaModelos;


namespace TalentosApp.Servicios.Base
{
    public class ServicioTalentos : IServicioTalentos
    {

        private readonly IUnidadTrabajo _unidadTrabajo;
        public ServicioTalentos(IUnidadTrabajo unidadTrabajo)
        {
            _unidadTrabajo = unidadTrabajo;
        }
        
        public IEnumerable<TalentosVw> ObtenerTalentos()
        {
            var _repoTalentos = _unidadTrabajo.RepositorioTalentos();
            var talentos = _repoTalentos.ObtenerRegistros(t => t.EstadoReg == true);
            if (talentos == null || !talentos.Any())
                return Enumerable.Empty<TalentosVw>();

            return talentos.Select(t => new TalentosVw()
            {
                TalentoID = t.TalentoID,
                NombreCompleto = t.NombreCompleto,
                NombreSimple = t.NombreSimple,
                PrimerNombre = t.PrimerNombre,
                PrimerApellido = t.PrimerApellido,
                SegundoNombre = t.SegundoNombre,
                SegundoApellido = t.SegundoApellido,
                Email = t.Email,
                FechaNacimiento = t.FechaNacimiento,
                Sexo = t.Sexo,
                DocumentoIdentidad = t.TalentosDocumentosIdentidad == null
                                    ? string.Empty : t.TalentosDocumentosIdentidad.DocumentoIdentidad,
                TipoDocumentoIdentidad = t.TalentosDocumentosIdentidad == null
                                    ? -1 : t.TalentosDocumentosIdentidad.TipoDocumentoIdentidad
            });
        }

        public int RegistrarTalentos(TalentoInputModel input)
        {
            if (input == null)
                return (int)ErroresComunes.ValoresNoValido;

            if (string.IsNullOrEmpty(input.PrimerNombre) || string.IsNullOrEmpty(input.PrimerApellido))
                return (int)ErroresComunes.CampoVacio;

            if (!Enum.IsDefined(typeof(Sexo), input.Sexo))
                return (int)ErroresComunes.CampoInvalidado;

            if (string.IsNullOrEmpty(input.Email))
                return (int)ErroresComunes.CampoVacio;

            if (!Enum.IsDefined(typeof(TipoDocumentoIdentidad), input.TipoDocumentoIdentidad))
                return (int)ErroresComunes.CampoInvalidado;

            if (string.IsNullOrEmpty(input.DocumentoIdentidad))
                return (int)ErroresComunes.CampoInvalidado;


            var documentoIdentidad = new TalentosDocumentosIdentidad()
            {
                DocumentoIdentidad = input.DocumentoIdentidad,
                TipoDocumentoIdentidad = input.TipoDocumentoIdentidad,
                EstadoReg = true,
                FechaModificacion = DateTime.Now,
                GuidReg = Guid.NewGuid()
            };

            var talentoDB = new Talentos()
            {
                PrimerNombre = input.PrimerNombre,
                SegundoNombre = input.SegundoNombre,
                PrimerApellido = input.PrimerApellido,
                SegundoApellido = input.SegundoApellido,
                Sexo = input.Sexo,
                FechaNacimiento = input.FechaNacimiento,
                Email = input.Email,
                TalentosDocumentosIdentidad = documentoIdentidad,
                EstadoReg = true,
                FechaModificacion = DateTime.Now,
                GuidReg = Guid.NewGuid()
            };


            var _repoTalentos = _unidadTrabajo.RepositorioTalentos();
            _repoTalentos.Insertar(talentoDB);
            var cambios = _unidadTrabajo.GuardarCambios();
            if (cambios)
                return talentoDB.TalentoID;
            return (int)ErroresComunes.ErrorGuardando;
            
        }

        public TalentosVw ObtenerTalento(int TalentoID)
        {
            var _repoTalentos = _unidadTrabajo.RepositorioTalentos();
            var talentoDB = _repoTalentos.ObtenerRegistro(t => t.EstadoReg == true &&  t.TalentoID == TalentoID);
            if (talentoDB == null)
                return null;

            var talentoAptitudes = talentoDB.TalentosAptitudes.Where(ta=> ta.EstadoReg == true).Select(ta => new TalentoAptitudViewModel()
            {
                Aptitud = ta.Aptitudes.Nombre,
                AptitudID = ta.AptitudID,
                NivelManejo = ta.NivelManejo,
                Talento = ta.Talentos.NombreSimple,
                TalentoID = ta.TalentoID,
                TiempoExperiencia = ta.TiempoExperiencia,
                UnidadTiempoExperiencia = ta.UnidadTiempoExperiencia,
                TalentoAptitudID = ta.TalentoAptitudID
            });

            return new TalentosVw() 
            { 
                TalentoID = talentoDB.TalentoID,
                NombreCompleto = talentoDB.NombreCompleto,
                NombreSimple = talentoDB.NombreSimple,
                PrimerNombre = talentoDB.PrimerNombre,
                PrimerApellido = talentoDB.PrimerApellido,
                SegundoNombre = talentoDB.SegundoNombre,
                SegundoApellido = talentoDB.SegundoApellido,
                Email = talentoDB.Email,
                FechaNacimiento = talentoDB.FechaNacimiento,
                Sexo = talentoDB.Sexo,
                Aptitudes = talentoAptitudes,
                DocumentoIdentidad = talentoDB.TalentosDocumentosIdentidad == null
                                    ? string.Empty: talentoDB.TalentosDocumentosIdentidad.DocumentoIdentidad,
                TipoDocumentoIdentidad = talentoDB.TalentosDocumentosIdentidad == null
                                    ? -1 : talentoDB.TalentosDocumentosIdentidad.TipoDocumentoIdentidad
            };
        }

        public int ModificarTalentos(TalentoInputModel input)
        {
            var _repoTalentos = _unidadTrabajo.RepositorioTalentos();
            var talentoDB = _repoTalentos.ObtenerRegistro(t => t.TalentoID == input.TalentoID);
            if (talentoDB == null)
                return (int)ErroresComunes.ValoresNoEncontrado;

            talentoDB.PrimerNombre = input.PrimerNombre;
            talentoDB.PrimerApellido = input.PrimerApellido;
            talentoDB.SegundoNombre = input.SegundoNombre;
            talentoDB.SegundoApellido = input.SegundoApellido;
            talentoDB.Sexo = input.Sexo;
            talentoDB.FechaNacimiento = input.FechaNacimiento;
            talentoDB.Email = input.Email;
            if (talentoDB.TalentosDocumentosIdentidad != null)
            {
                talentoDB.TalentosDocumentosIdentidad.TipoDocumentoIdentidad = input.TipoDocumentoIdentidad;
                talentoDB.TalentosDocumentosIdentidad.DocumentoIdentidad = input.DocumentoIdentidad;
            }
            
            var cambios = _unidadTrabajo.GuardarCambios();
            if (cambios)
                return talentoDB.TalentoID;
            return (int)ErroresComunes.ErrorGuardando;
        }

        public int EliminarTalento(int talentoID)
        {
            var _repoTalentos = _unidadTrabajo.RepositorioTalentos();
            var talentoDB = _repoTalentos.ObtenerRegistro(t => t.TalentoID == talentoID);
            if (talentoDB == null)
                return (int)ErroresComunes.ValoresNoEncontrado;

            talentoDB.EstadoReg = false;
            talentoDB.TalentosAptitudes.ToList().ForEach(t => t.EstadoReg = false);
            talentoDB.TalentosDocumentosIdentidad.EstadoReg = false;

            var cambios = _unidadTrabajo.GuardarCambios();
            if (cambios)
                return talentoDB.TalentoID;
            return (int)ErroresComunes.ErrorGuardando;
        }

        #region Aptitudes

        public int AgregarTalentoAptitud(TalentoAptitudInputModel entrada)
        {
            if (entrada == null)
                return (int)ErroresComunes.ValoresNoValido;            
            
            var esAptitudValida = _unidadTrabajo.RepositorioAptitudes()
                                        .CantidadRegistros(c => c.EstadoReg == true && c.AptitudID== entrada.AptitudID)>0;
            if (!esAptitudValida)
                return (int)ErroresComunes.ValoresNoValido;

            var _repoTalentos = _unidadTrabajo.RepositorioTalentos();
            var talentoDB = _repoTalentos.ObtenerRegistro(t => t.EstadoReg == true && t.TalentoID == entrada.TalentoID);

            if (talentoDB == null)
                return (int)ErroresComunes.ValoresNoValido;

            if (talentoDB.TalentosAptitudes != null && 
                talentoDB.TalentosAptitudes.Any(ta => ta.EstadoReg == true && ta.AptitudID == entrada.AptitudID))
                return (int)ErroresComunes.Duplicidad;

            var talentoAptitudDB = new TalentosAptitudes()
            {
                AptitudID = entrada.AptitudID,
                TalentoID = entrada.TalentoID,
                Talentos = talentoDB,
                NivelManejo = entrada.NivelManejo,
                TiempoExperiencia = entrada.TiempoExperiencia,
                UnidadTiempoExperiencia = entrada.UnidadTiempoExperiencia,
                ValoracionAptitud = entrada.ValoracionAptitud,
                EstadoReg = true,
                FechaModificacion = DateTime.Now,
                GuidReg = Guid.NewGuid()
            };
            talentoDB.TalentosAptitudes.Add(talentoAptitudDB);

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return talentoAptitudDB.TalentoAptitudID;
            
            return (int)ErroresComunes.ErrorGuardando;
        }

        public int ModificarTalentoAptitud(TalentoAptitudInputModel entrada)
        {
            if (entrada == null)
                return (int)ErroresComunes.ValoresNoValido;

            var _repoTalentosAptitudes = _unidadTrabajo.RepositorioTalentosAptitudes();
            var talentoAptitudDB = _repoTalentosAptitudes.ObtenerRegistro(ta => ta.EstadoReg == true 
                                    && ta.TalentoAptitudID == entrada.TalentoAptitudID);

            if (talentoAptitudDB == null)
                return (int)ErroresComunes.ValoresNoValido;

            var esAptitudValida = _unidadTrabajo.RepositorioAptitudes()
                                        .CantidadRegistros(c => c.EstadoReg == true && c.AptitudID == entrada.AptitudID) > 0;
            if (!esAptitudValida)
                return (int)ErroresComunes.ValoresNoValido;
            
            var _repoTalentos = _unidadTrabajo.RepositorioTalentos();
            var talentoDB = _repoTalentos.ObtenerRegistro(t => t.EstadoReg == true && t.TalentoID == entrada.TalentoID);

            if (talentoDB == null)
                return (int)ErroresComunes.ValoresNoValido;

            talentoAptitudDB.AptitudID = entrada.AptitudID;
            talentoAptitudDB.TalentoID = entrada.TalentoID;
            talentoAptitudDB.NivelManejo = entrada.NivelManejo;
            talentoAptitudDB.TiempoExperiencia = entrada.TiempoExperiencia;
            talentoAptitudDB.UnidadTiempoExperiencia = entrada.UnidadTiempoExperiencia;
            talentoAptitudDB.ValoracionAptitud = entrada.ValoracionAptitud;

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return talentoAptitudDB.TalentoAptitudID;

            return (int)ErroresComunes.ErrorGuardando;

        }

        public TalentoAptitudViewModel ObtenerTalentoAptitud(int talentoAptitudID)
        {
            var _repoTalentosAptitudes = _unidadTrabajo.RepositorioTalentosAptitudes();
            var talentoAptitud = _repoTalentosAptitudes.ObtenerRegistro(ta => ta.TalentoAptitudID == talentoAptitudID);
            if (talentoAptitud == null)
                return null;
            
            return new TalentoAptitudViewModel()
            {
                Aptitud = talentoAptitud.Aptitudes.Nombre,
                AptitudID = talentoAptitud.AptitudID,
                NivelManejo = talentoAptitud.NivelManejo,
                Talento = talentoAptitud.Talentos.NombreSimple,
                TalentoID = talentoAptitud.TalentoID,
                TiempoExperiencia = talentoAptitud.TiempoExperiencia,
                UnidadTiempoExperiencia = talentoAptitud.UnidadTiempoExperiencia,
                TalentoAptitudID = talentoAptitud.TalentoAptitudID,
                ValoracionAptitud = talentoAptitud.ValoracionAptitud
            };
        }

        

        public int RemoverTalentoApitutd(int talentoAptitudID)
        {
            var _repoTalentosAptitudes = _unidadTrabajo.RepositorioTalentosAptitudes();
            var talentoAptitud = _repoTalentosAptitudes.ObtenerRegistro(ta => ta.TalentoAptitudID == talentoAptitudID);
            if (talentoAptitud == null)
                return (int)ErroresComunes.ValoresNoEncontrado;

            talentoAptitud.EstadoReg = false;
            talentoAptitud.FechaModificacion = DateTime.Now;
            
            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return talentoAptitud.TalentoAptitudID;

            return (int)ErroresComunes.ErrorGuardando;
        }

        #endregion
    }
}
