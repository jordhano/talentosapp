﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contractos.AccesoDatos.Repositorios;
using TalentosApp.Contractos.Servicios;
using TalentosApp.Contratos.AccesoDatos;
using TalentosApp.Contratos.AccesoDatos.Repositorios;
using TalentosApp.Enumeradores;
using TalentosApp.Nucleo.Entidades;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.TransferenciaDatos.VistaModelos;

namespace TalentosApp.Servicios.Base
{
    public class ServicioClientes : IServicioClientes
    {
        private readonly IUnidadTrabajo _unidadTrabajo;
        public ServicioClientes(IUnidadTrabajo unidadTrabajo)
        {
            _unidadTrabajo = unidadTrabajo;
        }

        public IEnumerable<ClientesVw> ObtenerClientes()
        {
            var _repoBaseClientes = _unidadTrabajo.RepositorioClientes();
            var clientes = _repoBaseClientes.ObtenerRegistros(c => c.EstadoReg == true);
            var existenResultados = clientes != null && clientes.Any();

            if (!existenResultados)
                return Enumerable.Empty<ClientesVw>();

            return clientes.Select(c => new ClientesVw() { 
                ClienteID = c.ClienteID,
                Nombre = c.Nombre
            });
        }

        public int RegistrarCliente(ClienteInputModel entrada)
        {
            if (entrada == null)
                return (int)ErroresComunes.CampoInvalidado;

            var esNombreValido = !string.IsNullOrEmpty(entrada.Nombre);
            if (esNombreValido)
                return (int)ErroresComunes.CampoVacio;

            var clienteDB = new Clientes()
            {
                Nombre = entrada.Nombre,
                EstadoReg = true,
                GuidReg = Guid.NewGuid(),
                FechaModificacion = DateTime.Now
            };

            var _repoBaseClientes = _unidadTrabajo.RepositorioClientes();
            _repoBaseClientes.Insertar(clienteDB);

            var seGuardanCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardanCambios)
                return clienteDB.ClienteID;
            else
                return (int)ErroresComunes.ErrorGuardando;
        }

        public ClientesVw ObtenerCliente(int id)
        {
            var _repoBaseClientes = _unidadTrabajo.RepositorioClientes();
            var clienteDB = _repoBaseClientes.ObtenerRegistro(c => c.EstadoReg == true && c.ClienteID == id);
            if (clienteDB == null)
                return null;

            return new ClientesVw()
            {
                ClienteID = clienteDB.ClienteID,
                Nombre = clienteDB.Nombre
            };
        }

        public int ModificarCliente(ClienteInputModel entrada)
        {
            if (entrada== null)
                return (int)ErroresComunes.CampoInvalidado;

            var _repoBaseClientes = _unidadTrabajo.RepositorioClientes();
            var clienteDB = _repoBaseClientes.ObtenerRegistro(c => c.EstadoReg == true && c.ClienteID == entrada.ClienteID);

            var existeCliente = clienteDB != null;
            if (!existeCliente)
                return (int)ErroresComunes.ValoresNoEncontrado;

            var esNombreValido = !string.IsNullOrEmpty(entrada.Nombre);
            if (!esNombreValido)
                return (int)ErroresComunes.CampoVacio;

            clienteDB.Nombre = entrada.Nombre;

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return entrada.ClienteID;

            return (int)ErroresComunes.ErrorGuardando;   
        }

        public int RemoverCliente(int clienteID)
        {
            var _repoBaseClientes = _unidadTrabajo.RepositorioClientes();
            var clienteDB = _repoBaseClientes.ObtenerRegistro(c => c.EstadoReg == true && c.ClienteID == clienteID);
            
            var existeCliente = clienteDB != null;
            if (!existeCliente)
                return (int)ErroresComunes.ValoresNoEncontrado;

            clienteDB.EstadoReg = false;
            clienteDB.Vacantes.ToList().ForEach(v => v.EstadoReg = false);

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return clienteID;
            
            return (int)ErroresComunes.ErrorGuardando;
        }
    }
}
