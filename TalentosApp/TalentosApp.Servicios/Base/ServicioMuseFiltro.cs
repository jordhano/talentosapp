﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contractos.AccesoDatos.Repositorios;
using TalentosApp.Contractos.Servicios;
using TalentosApp.Contratos.AccesoDatos;
using TalentosApp.Enumeradores;
using TalentosApp.Nucleo.Entidades;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.TransferenciaDatos.VistaModelos;
using TalentosApp.TransferenciaDatos.VistaModelos.MuseFiltro;

namespace TalentosApp.Servicios.Base
{
    public class ServicioMuseFiltro : IServicioMuseFiltro
    {
        private readonly IUnidadTrabajo _unidadTrabajo;
        public ServicioMuseFiltro(IUnidadTrabajo unidadTrabajo)
        {
            _unidadTrabajo = unidadTrabajo;
        }
        public AptitudesMuseResultadoVw ObtenerUsandoAptitudes(MuseFiltroInputModel entrada)
        {
            if (entrada.Aptitudes == null)
                return new AptitudesMuseResultadoVw()
                {
                    TotalFiltros = 0,
                    Talentos = Enumerable.Empty<EntidadMuseResultadoVw>(),
                    Perfiles = Enumerable.Empty<EntidadMuseResultadoVw>(),
                    Vacantes = Enumerable.Empty<EntidadMuseResultadoVw>()
                };

            var resultado = new AptitudesMuseResultadoVw()
            {
                TotalFiltros = entrada.Aptitudes.Count()
            };

            var _repositorioMuseFiltro = _unidadTrabajo.RepositorioMuseFiltro();
            var tuplas = _repositorioMuseFiltro.FiltroPorAptitudes<TuplaMuseResultadoVw>(entrada.Aptitudes);

            resultado.Talentos = TuplasTalentosParaAptitudes(tuplas);
            resultado.Perfiles = TuplasPerfilesParaAptitudes(tuplas);
            resultado.Vacantes = TuplasVacantesParaAptitudes(tuplas);
            return resultado;

        }

        private List<EntidadMuseResultadoVw> TuplasVacantesParaAptitudes(IEnumerable<TuplaMuseResultadoVw> tuplas)
        {
            var tuplasVacantes = tuplas.Where(t => t.TipoEntidad == (int)TipoEntidadFiltroMuse.Vacante);
            var existenTuplasVacantes = tuplasVacantes != null && tuplasVacantes.Any();
            
            if (!existenTuplasVacantes)
                return Enumerable.Empty<EntidadMuseResultadoVw>().ToList();

            var entidades = new List<EntidadMuseResultadoVw>();
            var iteradorVacantes = from vacante in tuplasVacantes
                                    group vacante by vacante.EntityID into vacante
                                    select new { VacanteID = vacante.Key, Aptitudes = vacante };

            foreach (var vacantes in iteradorVacantes)
            {
                var vacanteMuse = new EntidadMuseResultadoVw();
                foreach (var aptitud in vacantes.Aptitudes)
                {
                    vacanteMuse.Nombre = aptitud.Nombre;
                    vacanteMuse.Ranking = aptitud.ResultadoEval;
                    var aptitudMuse = new AptitudMuseResultadoVw()
                    {
                        Nombre = aptitud.Aptitud,
                        Estado = aptitud.Estado
                    };
                    if (aptitud.Estado == (int)EstadoFiltroMuse.NoCumple)
                        vacanteMuse.Miscelaneos.Add(aptitudMuse);
                    else
                        vacanteMuse.Aplica.Add(aptitudMuse);
                }
                entidades.Add(vacanteMuse);
            }
            entidades = entidades.OrderByDescending(t => t.Ranking).ToList();
            return entidades;
        }

        private List<EntidadMuseResultadoVw> TuplasPerfilesParaAptitudes(IEnumerable<TuplaMuseResultadoVw> tuplas)
        {
            var tuplasPerfiles = tuplas.Where(t => t.TipoEntidad == (int)TipoEntidadFiltroMuse.Perfil);
            var existenTuplasPerfiles = tuplasPerfiles != null && tuplasPerfiles.Any();

            if (!existenTuplasPerfiles)
                return Enumerable.Empty<EntidadMuseResultadoVw>().ToList();

            var entidades = new List<EntidadMuseResultadoVw>();
            if (tuplasPerfiles != null && tuplasPerfiles.Any())
            {
                var iteradorPerfiles = from perfil in tuplasPerfiles
                                       group perfil by perfil.EntityID into perfil
                                       select new { PerfilID = perfil.Key, Aptitudes = perfil };

                foreach (var perfil in iteradorPerfiles)
                {
                    var perfilMuse = new EntidadMuseResultadoVw();
                    foreach (var aptitud in perfil.Aptitudes)
                    {
                        perfilMuse.Nombre = aptitud.Nombre;
                        perfilMuse.Ranking = aptitud.ResultadoEval;
                        var aptitudMuse = new AptitudMuseResultadoVw()
                        {
                            Nombre = aptitud.Aptitud,
                            Estado = aptitud.Estado
                        };
                        if (aptitud.Estado == (int)EstadoFiltroMuse.NoCumple)
                            perfilMuse.Miscelaneos.Add(aptitudMuse);
                        else
                            perfilMuse.Aplica.Add(aptitudMuse);
                    }
                    entidades.Add(perfilMuse);
                }
                entidades = entidades.OrderByDescending(t => t.Ranking).ToList();
            }
            return entidades;
        }

        private List<EntidadMuseResultadoVw> TuplasTalentosParaAptitudes(IEnumerable<TuplaMuseResultadoVw> tuplas)
        {
            var tuplasTalentos = tuplas.Where(t => t.TipoEntidad == (int)TipoEntidadFiltroMuse.Talento);
            var existenTuplasTalentos = tuplasTalentos != null && tuplasTalentos.Any();

            if (!existenTuplasTalentos)
                return Enumerable.Empty<EntidadMuseResultadoVw>().ToList();

            var entidades = new List<EntidadMuseResultadoVw>();
            if (tuplasTalentos != null && tuplasTalentos.Any())
            {
                var iteradorTalentos = from talento in tuplasTalentos
                                       group talento by talento.EntityID into talento
                                       select new { TalentoID = talento.Key, Aptitudes = talento };

                foreach (var talento in iteradorTalentos)
                {
                    var talentoMuse = new EntidadMuseResultadoVw();
                    foreach (var aptitud in talento.Aptitudes)
                    {
                        talentoMuse.Nombre = aptitud.Nombre;
                        talentoMuse.Ranking = aptitud.ResultadoEval;
                        var aptitudMuse = new AptitudMuseResultadoVw()
                        {
                            Nombre = aptitud.Aptitud,
                            Estado = aptitud.Estado
                        };
                        if (aptitud.Estado == (int)EstadoFiltroMuse.NoCumple)
                            talentoMuse.Miscelaneos.Add(aptitudMuse);
                        else
                            talentoMuse.Aplica.Add(aptitudMuse);
                    }
                    entidades.Add(talentoMuse);
                }
                entidades = entidades.OrderByDescending(t => t.Ranking).ToList();
            }
            return entidades;
        }

        public TalentoMuseResultadoVw ObtenerUsandoTalento(MuseFiltroInputModel entrada)
        {
            if (!entrada.Talento.HasValue)
                return new TalentoMuseResultadoVw()
                {
                    TotalFiltros = 0,
                    Perfiles = Enumerable.Empty<EntidadMuseResultadoVw>(),
                    Vacantes = Enumerable.Empty<EntidadMuseResultadoVw>(),
                    Aptitudes = Enumerable.Empty<AptitudesVw>()
                };


            var aptitudes = _unidadTrabajo.RepositorioTalentosAptitudes()
                    .ObtenerRegistros(t => t.TalentoID == entrada.Talento.Value && t.EstadoReg == true);
            var idsAPtitudes = aptitudes.Select(t => t.AptitudID).Distinct().ToArray();

            entrada.Aptitudes = idsAPtitudes;

            var _repositorioMuseFiltro = _unidadTrabajo.RepositorioMuseFiltro();
            var tuplas = _repositorioMuseFiltro.FiltroPorAptitudesTalento<TuplaMuseResultadoVw>(entrada.Talento.Value);
            var perfiles = new List<EntidadMuseResultadoVw>();
            var vacantes = new List<EntidadMuseResultadoVw>();


            var tuplasPerfiles = tuplas.Where(t => t.TipoEntidad == (int)TipoEntidadFiltroMuse.Perfil);
            if (tuplasPerfiles != null && tuplasPerfiles.Any())
            {
                var iteradorPerfiles = from perfil in tuplasPerfiles
                                       group perfil by perfil.EntityID into perfil
                                       select new { PerfilID = perfil.Key, Aptitudes = perfil };

                foreach (var perfil in iteradorPerfiles)
                {
                    var perfilMuse = new EntidadMuseResultadoVw();
                    foreach (var aptitud in perfil.Aptitudes)
                    {
                        perfilMuse.Nombre = aptitud.Nombre;
                        perfilMuse.Ranking = aptitud.ResultadoEval;
                        var aptitudMuse = new AptitudMuseResultadoVw()
                        {
                            Nombre = aptitud.Aptitud,
                            Estado = aptitud.Estado
                        };
                        switch ((EstadoFiltroMuse)aptitud.Estado)
                        {
                            case EstadoFiltroMuse.Cumple:
                                perfilMuse.Aplica.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.Maneja:
                                perfilMuse.Manejo.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.Tiempo:
                                perfilMuse.Tiempo.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.Misc:
                                perfilMuse.Miscelaneos.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.NoCumple:
                                perfilMuse.NA.Add(aptitudMuse);
                                break;
                        }
                    }
                    perfiles.Add(perfilMuse);
                }
                perfiles = perfiles.OrderByDescending(t => t.Ranking).ToList();
            }

            var tuplasVacantes = tuplas.Where(t => t.TipoEntidad == (int)TipoEntidadFiltroMuse.Vacante);
            if (tuplasVacantes != null && tuplasVacantes.Any())
            {
                var iteradorVacantes = from vacante in tuplasVacantes
                                       group vacante by vacante.EntityID into vacante
                                       select new { VacanteID = vacante.Key, Aptitudes = vacante };

                foreach (var vacante in iteradorVacantes)
                {
                    var vacanteMuse = new EntidadMuseResultadoVw();
                    foreach (var aptitud in vacante.Aptitudes)
                    {
                        vacanteMuse.Nombre = aptitud.Nombre;
                        vacanteMuse.Ranking = aptitud.ResultadoEval;
                        var aptitudMuse = new AptitudMuseResultadoVw()
                        {
                            Nombre = aptitud.Aptitud,
                            Estado = aptitud.Estado
                        };
                        switch ((EstadoFiltroMuse)aptitud.Estado)
                        {
                            case EstadoFiltroMuse.Cumple:
                                vacanteMuse.Aplica.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.Maneja:
                                vacanteMuse.Manejo.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.Tiempo:
                                vacanteMuse.Tiempo.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.Misc:
                                vacanteMuse.Miscelaneos.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.NoCumple:
                                vacanteMuse.NA.Add(aptitudMuse);
                                break;
                        }
                    }
                    vacantes.Add(vacanteMuse);
                }
                vacantes = vacantes.OrderByDescending(t => t.Ranking).ToList();
            }


            //var resultadoParcial = ObtenerUsandoAptitudes(entrada);

            return new TalentoMuseResultadoVw()
            {
                TotalFiltros = idsAPtitudes.Length,
                Perfiles = perfiles,
                Vacantes = vacantes,
                Aptitudes = aptitudes.Select(a => new AptitudesVw()
                {
                    Nombre = a.Aptitudes.Nombre,
                    AptitudID = a.AptitudID
                }).ToList()
            };

        }


        //public TalentoMuseResultadoVw ObtenerUsandoTalento2(MuseFiltroInputModel entrada)
        //{
        //    if (!entrada.Talento.HasValue)
        //        return new TalentoMuseResultadoVw()
        //        {
        //            TotalFiltros = 0,
        //            Perfiles = Enumerable.Empty<EntidadMuseResultadoVw>(),
        //            Vacantes = Enumerable.Empty<EntidadMuseResultadoVw>(),
        //            Aptitudes = Enumerable.Empty<AptitudesVw>()
        //        };


        //    var aptitudes = _unidadTrabajo.RepositorioTalentosAptitudes()
        //            .ObtenerRegistros(t => t.TalentoID == entrada.Talento.Value && t.EstadoReg == true);
        //    var idsAPtitudes = aptitudes.Select(t => t.AptitudID).Distinct().ToArray();

        //    entrada.Aptitudes = idsAPtitudes;
        //    var resultadoParcial = ObtenerUsandoAptitudes(entrada);

        //    return new TalentoMuseResultadoVw()
        //    {
        //        TotalFiltros = resultadoParcial.TotalFiltros,
        //        Perfiles = resultadoParcial.Perfiles,
        //        Vacantes = resultadoParcial.Perfiles,
        //        Aptitudes = aptitudes.Select(a => new AptitudesVw() {
        //            Nombre = a.Aptitudes.Nombre, 
        //            AptitudID = a.AptitudID}).ToList()
        //    };

        //}


        public PerfilesMuseResultadoVw ObtenerUsandoPerfiles(MuseFiltroInputModel entrada)
        {
            if (entrada.Perfiles == null)
                return new PerfilesMuseResultadoVw()
                {
                    TotalFiltros = 0,
                    Talentos = Enumerable.Empty<EntidadMuseResultadoVw>(),
                    Aptitudes = Enumerable.Empty<AptitudesVw>()
                };

            var aptitudes = _unidadTrabajo.RepositorioPerfilesAptitudes()
                    .ObtenerRegistros(t => entrada.Perfiles.Any(p => p == t.PerfilID) && t.EstadoReg == true);
            var idsAPtitudes = aptitudes.Select(t => t.AptitudID).Distinct().ToArray();

            entrada.Aptitudes = idsAPtitudes;
            //var resultadoParcial = ObtenerUsandoAptitudes(entrada);
            
            var _repositorioMuseFiltro = _unidadTrabajo.RepositorioMuseFiltro();
            var tuplas = _repositorioMuseFiltro.FiltroPorAptitudesPerfil<TuplaMuseResultadoVw>(entrada.Perfiles);
            var entidades = new List<EntidadMuseResultadoVw>();

            var tuplasTalentos = tuplas.Where(t => t.TipoEntidad == (int)TipoEntidadFiltroMuse.Talento);
            if (tuplasTalentos != null && tuplasTalentos.Any())
            {
                var iteradorTalentos = from talento in tuplasTalentos
                                       group talento by talento.EntityID into talento
                                       select new { TalentoID = talento.Key, Aptitudes = talento };

                foreach (var talento in iteradorTalentos)
                {
                    var talentoMuse = new EntidadMuseResultadoVw();
                    foreach (var aptitud in talento.Aptitudes)
                    {
                        talentoMuse.Nombre = aptitud.Nombre;
                        talentoMuse.Ranking = aptitud.ResultadoEval;
                        var aptitudMuse = new AptitudMuseResultadoVw()
                        {
                            Nombre = aptitud.Aptitud,
                            Estado = aptitud.Estado
                        };
                        switch ((EstadoFiltroMuse)aptitud.Estado)
                        {
                            case EstadoFiltroMuse.Cumple:
                                talentoMuse.Aplica.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.Maneja:
                                talentoMuse.Manejo.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.Tiempo:
                                talentoMuse.Tiempo.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.Misc:
                                talentoMuse.Miscelaneos.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.NoCumple:
                                talentoMuse.NA.Add(aptitudMuse);
                                break;
                        }

                    }
                    entidades.Add(talentoMuse);
                }
                entidades = entidades.OrderByDescending(t => t.Ranking).ToList();
            }

            return new PerfilesMuseResultadoVw()
            {
                TotalFiltros = entrada.Aptitudes.Length,
                Talentos = entidades,
                Aptitudes = aptitudes.Select(a => new AptitudesVw() { 
                    Nombre = a.Aptitudes.Nombre, 
                    AptitudID = a.AptitudID }).ToList()
            };
        }


        public VacanteMuseResultadoVw ObtenerUsandoVacante(MuseFiltroInputModel entrada)
        {
            if (!entrada.Vacante.HasValue)
                return new VacanteMuseResultadoVw()
                {
                    TotalFiltros = 0,
                    Talentos = Enumerable.Empty<EntidadMuseResultadoVw>(),
                    Aptitudes = Enumerable.Empty<AptitudesVw>()
                };

            var aptitudes = _unidadTrabajo.RepositorioVacantesAptitudes()
                    .ObtenerRegistros(t => t.VacanteID == entrada.Vacante.Value && t.EstadoReg == true).ToList();
            var idsAPtitudes = aptitudes.Select(t => t.AptitudID).Distinct().ToList();

            var perfiles = _unidadTrabajo.RepositorioVacantesPerfiles()
                    .ObtenerRegistros(t => t.VacanteID == entrada.Vacante.Value && t.EstadoReg == true)
                    .Select(p => p.PerfilID);
            
            var perfilesAptitudes = _unidadTrabajo.RepositorioPerfilesAptitudes()
                   .ObtenerRegistros(t => perfiles.Any(p => p == t.PerfilID) 
                                        && !idsAPtitudes.Any(a => a == t.AptitudID) 
                                        && t.EstadoReg == true).ToList();
            idsAPtitudes.AddRange(perfilesAptitudes.Select(p => p.AptitudID).ToList());

           var aptitudesVw = aptitudes.Select(a => new AptitudesVw()
                {
                    Nombre = a.Aptitudes.Nombre,
                    AptitudID = a.AptitudID
                }).ToList();

            aptitudesVw.AddRange( perfilesAptitudes.Select (pa => new AptitudesVw(){
                Nombre = pa.Aptitudes.Nombre,
                AptitudID = pa.AptitudID
            }));
            
            entrada.Aptitudes = idsAPtitudes.ToArray();
            var _repositorioMuseFiltro = _unidadTrabajo.RepositorioMuseFiltro();
            var tuplas = _repositorioMuseFiltro.FiltroPorAptitudesVacante<TuplaMuseResultadoVw>(entrada.Vacante.Value);
            var entidades = new List<EntidadMuseResultadoVw>();

            var tuplasTalentos = tuplas.Where(t => t.TipoEntidad == (int)TipoEntidadFiltroMuse.Talento);
            if (tuplasTalentos != null && tuplasTalentos.Any())
            {
                var iteradorTalentos = from talento in tuplasTalentos
                                       group talento by talento.EntityID into talento
                                       select new { TalentoID = talento.Key, Aptitudes = talento };

                foreach (var talento in iteradorTalentos)
                {
                    var talentoMuse = new EntidadMuseResultadoVw();
                    foreach (var aptitud in talento.Aptitudes)
                    {
                        talentoMuse.Nombre = aptitud.Nombre;
                        talentoMuse.Ranking += aptitud.Orden.HasValue ? aptitud.Orden.Value : 0;
                        var aptitudMuse = new AptitudMuseResultadoVw()
                        {
                            Nombre = aptitud.Aptitud,
                            Estado = aptitud.Estado
                        };
                        switch ((EstadoFiltroMuse)aptitud.Estado)
                        {
                            case EstadoFiltroMuse.Cumple:
                                talentoMuse.Aplica.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.Maneja:
                                talentoMuse.Manejo.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.Tiempo:
                                talentoMuse.Tiempo.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.Misc:
                                talentoMuse.Miscelaneos.Add(aptitudMuse);
                                break;
                            case EstadoFiltroMuse.NoCumple:
                                talentoMuse.NA.Add(aptitudMuse);
                                break;
                        }
                            
                    }
                    entidades.Add(talentoMuse);
                }
                var bases = entidades.Where(e => e.Ranking == 0);
                entidades = entidades.Where(e => e.Ranking>0).OrderBy(t => t.Ranking).ToList();
                entidades.AddRange(bases);
            }

            return new VacanteMuseResultadoVw()
            {
                TotalFiltros = entrada.Aptitudes.Length,
                Talentos = entidades,
                Aptitudes = aptitudesVw
            };
        }
    }
}
