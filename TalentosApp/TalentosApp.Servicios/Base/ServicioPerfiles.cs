﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contractos.AccesoDatos.Repositorios;
using TalentosApp.Contractos.Servicios;
using TalentosApp.Contratos.AccesoDatos;
using TalentosApp.Contratos.AccesoDatos.Repositorios;
using TalentosApp.Enumeradores;
using TalentosApp.Nucleo.Entidades;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.TransferenciaDatos.VistaModelos;

namespace TalentosApp.Servicios.Base
{
    public class ServicioPerfiles : IServicioPerfiles
    {
        private readonly IUnidadTrabajo _unidadTrabajo;
        public ServicioPerfiles(IUnidadTrabajo unidadTrabajo)
        {
            _unidadTrabajo = unidadTrabajo;
            var _repoPerfiles = _unidadTrabajo.RepositorioPerfiles();
            var _repoPerfilesAptitudes = _unidadTrabajo.RepositorioPerfilesAptitudes();
        }

        public int RegistrarPerfil(PerfilInputModel entrada)
        {
            if (entrada.Nombre.Trim() == string.Empty)
                return (int)ErroresComunes.CampoVacio;
            
            var _repoPerfiles = _unidadTrabajo.RepositorioPerfiles();
            var existente = _repoPerfiles.CantidadRegistros(p => 
                                                string.Compare(p.Nombre, entrada.Nombre, true) == 0
                                                && p.EstadoReg == true);
            if (existente > 0)
                return (int) ErroresComunes.Duplicidad;

            var PerfilBD = new Perfiles()
            {
                Nombre = entrada.Nombre,
                Descripcion = entrada.Descripcion,
                FechaModificacion = DateTime.Now,
                GuidReg = Guid.NewGuid(),
                EstadoReg = true
            };

            _repoPerfiles.Insertar(PerfilBD);

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return PerfilBD.PerfilID;
            
            return (int)ErroresComunes.ErrorGuardando;

        }

        public int AgregarAptitudPerfil(PerfilAptitudInputModel entrada)
        {
            var _repoPerfiles = _unidadTrabajo.RepositorioPerfiles();
            var perfil = _repoPerfiles.ObtenerRegistro(p => p.PerfilID == entrada.PerfilID);
            if (perfil == null)
                return (int)ErroresComunes.ValoresNoEncontrado;

            var _repoAptitudes = _unidadTrabajo.RepositorioAptitudes();
            var aptitud = _repoAptitudes.ObtenerRegistro(p => p.AptitudID == entrada.AptitudID);
            if (aptitud == null)
                return (int)ErroresComunes.ValoresNoEncontrado;

            if (entrada.TiempoExperiencia < 1)
                return (int)ErroresComunes.CampoInvalidado;

            if (perfil.PerfilesAptitudes != null 
                && perfil.PerfilesAptitudes.Any(pa => pa.EstadoReg == true && pa.AptitudID == entrada.AptitudID))
                return (int)ErroresComunes.Duplicidad;

            var perfilAptitud = new PerfilesAptitudes()
            {
                AptitudID = entrada.AptitudID,
                NivelManejo = entrada.NivelManejo,
                TiempoExperiencia = entrada.TiempoExperiencia,
                UnidadTiempoExperiencia = entrada.UnidadTiempoExperiencia,
                FechaModificacion = DateTime.Now,
                EstadoReg = true,
                GuidReg = Guid.NewGuid()
            };
            perfil.PerfilesAptitudes.Add(perfilAptitud);


            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return perfilAptitud.PerfilAptitudID;

            return (int)ErroresComunes.ErrorGuardando;
        }




        public PerfilViewModel ObtenerPerfil(int perfilID)
        {
            var _repoPerfiles = _unidadTrabajo.RepositorioPerfiles();
            var perfil = _repoPerfiles.ObtenerRegistro(p => p.EstadoReg == true && p.PerfilID == perfilID);
            if (perfil == null)
                return null;

            var aptitudes = perfil.PerfilesAptitudes != null 
                            && perfil.PerfilesAptitudes.Any(pa => pa.EstadoReg == true)?
                                        perfil.PerfilesAptitudes.Where(p=> p.EstadoReg == true)
                                        .Select(pa=>  
                                            new PerfilAptitudViewModel(){
                                                Aptitud = pa.Aptitudes.Nombre,
                                                AptitudID = pa.AptitudID,
                                                NivelManejo = pa.NivelManejo,
                                                Perfil = perfil.Nombre,
                                                PerfilID = perfil.PerfilID,
                                                TiempoExperiencia = pa.TiempoExperiencia,
                                                UnidadTiempoExperiencia = pa.UnidadTiempoExperiencia,
                                                PerfilAptitudID = pa.PerfilAptitudID
                                            }) : Enumerable.Empty < PerfilAptitudViewModel>();

            return new PerfilViewModel() { 
                PerfilID = perfil.PerfilID,
                Nombre = perfil.Nombre,
                Descripcion = perfil.Descripcion,
                AptitudesPerfil = aptitudes
                                                                    
            };
        }


        public PerfilAptitudViewModel ObtenerPerfilAptitud(int perfilAptitudID)
        {
            var _repoPerfilesAptitudes = _unidadTrabajo.RepositorioPerfilesAptitudes();
            var perfilAptitud = _repoPerfilesAptitudes.ObtenerRegistro(pa => pa.PerfilAptitudID == perfilAptitudID);
            if (perfilAptitud == null)
                return null;

            return new PerfilAptitudViewModel()
            {
                Aptitud = perfilAptitud.Aptitudes.Nombre,
                AptitudID = perfilAptitud.AptitudID,
                NivelManejo = perfilAptitud.NivelManejo,
                Perfil = perfilAptitud.Perfiles.Nombre,
                PerfilID = perfilAptitud.PerfilID,
                TiempoExperiencia = perfilAptitud.TiempoExperiencia,
                UnidadTiempoExperiencia = perfilAptitud.UnidadTiempoExperiencia,
                PerfilAptitudID = perfilAptitud.PerfilAptitudID
            };


        }


        public int ModificarAptitudPerfil(PerfilAptitudInputModel entrada)
        {
            var _repoPerfilesAptitudes = _unidadTrabajo.RepositorioPerfilesAptitudes();
            var perfilAptitudDB = _repoPerfilesAptitudes.ObtenerRegistro(pa => pa.PerfilAptitudID == entrada.PerfilAptitudID); 
            if (perfilAptitudDB == null)
                return (int)ErroresComunes.ValoresNoEncontrado;

            if (perfilAptitudDB.PerfilID != entrada.PerfilID)
            {
                var _repoPerfiles = _unidadTrabajo.RepositorioPerfiles();
                var perfil = _repoPerfiles.ObtenerRegistro(p => p.PerfilID == entrada.PerfilID);
                if (perfil == null)
                    return (int)ErroresComunes.ValoresNoEncontrado;
                perfilAptitudDB.PerfilID = entrada.PerfilID;
            }

            if (perfilAptitudDB.AptitudID != entrada.AptitudID)
            {

                var _repoAptitudes = _unidadTrabajo.RepositorioAptitudes();
                var aptitud = _repoAptitudes.ObtenerRegistro(p => p.AptitudID == entrada.AptitudID);
                if (aptitud == null)
                    return (int)ErroresComunes.ValoresNoEncontrado;
                perfilAptitudDB.AptitudID = entrada.AptitudID;
            }

            if (entrada.TiempoExperiencia < 1)
                return (int)ErroresComunes.CampoInvalidado;

            
            perfilAptitudDB.NivelManejo = entrada.NivelManejo;
            perfilAptitudDB.TiempoExperiencia = entrada.TiempoExperiencia;
            perfilAptitudDB.UnidadTiempoExperiencia = entrada.UnidadTiempoExperiencia;
            perfilAptitudDB.FechaModificacion = DateTime.Now;


            var cambios = _unidadTrabajo.GuardarCambios();
            if (cambios)
                return entrada.PerfilAptitudID;

            return (int)ErroresComunes.ErrorGuardando;
        }


        public int RemoverAptitudPerfil(int aptitudPerfilID)
        {
            var _repoPerfilesAptitudes = _unidadTrabajo.RepositorioPerfilesAptitudes();
            var perfilAptitud = _repoPerfilesAptitudes.ObtenerRegistro(pa => pa.PerfilAptitudID == aptitudPerfilID);
            if (perfilAptitud == null)
                return (int)ErroresComunes.ValoresNoEncontrado;

            perfilAptitud.EstadoReg = false;
            perfilAptitud.FechaModificacion = DateTime.Now;
            perfilAptitud.VacantePriorizacion.ToList().ForEach(vp => vp.EstadoReg = false);

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return perfilAptitud.PerfilAptitudID;

            return (int)ErroresComunes.ErrorGuardando;

        }


        public IEnumerable<PerfilViewModel> ObtenerPerfiles()
        {
            var _repoPerfiles = _unidadTrabajo.RepositorioPerfiles();
            var perfiles = _repoPerfiles.ObtenerRegistros(p => p.EstadoReg == true);
            if (perfiles == null)
                return Enumerable.Empty<PerfilViewModel>();


            return perfiles.Select(p=> new PerfilViewModel()
            {
                PerfilID = p.PerfilID,
                Nombre = p.Nombre,
                Descripcion = p.Descripcion
            });
        }


        public IEnumerable<PerfilViewModel> ObtenerPerfilesDisponibleVacantes(int vacanteID)
        {
            var _repoPerfiles = _unidadTrabajo.RepositorioPerfiles();
            var perfiles = _repoPerfiles.ObtenerPerfilesDisponiblesVacante<PerfilViewModel>(vacanteID);
            if (perfiles == null || !perfiles.Any())
                return Enumerable.Empty<PerfilViewModel>();

            return perfiles;
        }


        public int ModificarPerfil(PerfilInputModel entrada)
        {
            var _repoPerfiles = _unidadTrabajo.RepositorioPerfiles();
            if (entrada.Nombre.Trim() == string.Empty)
                return (int)ErroresComunes.CampoVacio;

            var existente = _repoPerfiles.CantidadRegistros(p => 
                                                string.Compare(p.Nombre, entrada.Nombre, true) == 0
                                                && p.EstadoReg == true
                                                && p.PerfilID != entrada.PerfilID);
            if (existente > 0)
                return (int)ErroresComunes.Duplicidad;

            var perfilBD = _repoPerfiles.ObtenerRegistro(p => p.EstadoReg == true 
                                                        && p.PerfilID == entrada.PerfilID);
            if (perfilBD == null)
                return (int)ErroresComunes.ValoresNoEncontrado;


            perfilBD.Nombre = entrada.Nombre;
            perfilBD.Descripcion = entrada.Descripcion;
            perfilBD.FechaModificacion = DateTime.Now;

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return perfilBD.PerfilID;

            return (int)ErroresComunes.ErrorGuardando;
        }


        public int RemoverPerfil(int perfilID)
        {
            var _repoPerfiles = _unidadTrabajo.RepositorioPerfiles();
            var perfil = _repoPerfiles.ObtenerRegistro(pa => pa.PerfilID == perfilID);
            if (perfil == null)
                return (int)ErroresComunes.ValoresNoEncontrado;

            perfil.EstadoReg = false;
            perfil.FechaModificacion = DateTime.Now;
            perfil.PerfilesAptitudes.ToList().ForEach(p => p.EstadoReg = false);
            perfil.VacantesPerfiles.ToList().ForEach(p => p.EstadoReg = false);
            perfil.PerfilesAptitudes.ToList()
                    .ForEach(v=> v.VacantePriorizacion.ToList()
                                    .ForEach(vp => vp.EstadoReg = false));


            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return perfil.PerfilID;

            return (int)ErroresComunes.ErrorGuardando;
        }
    }
}
