﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contractos.AccesoDatos.Repositorios;
using TalentosApp.Contractos.Servicios;
using TalentosApp.Contratos.AccesoDatos;
using TalentosApp.Contratos.AccesoDatos.Repositorios;
using TalentosApp.Enumeradores;
using TalentosApp.Nucleo.Entidades;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.TransferenciaDatos.VistaModelos;

namespace TalentosApp.Servicios.Base
{
    public class ServicioVacantes : IServicioVacantes
    {

        private readonly IUnidadTrabajo _unidadTrabajo;
        public ServicioVacantes(IUnidadTrabajo unidadTrabajo)
        {
            _unidadTrabajo = unidadTrabajo;
        }
        #region CRUD-Basico
        public IEnumerable<VacantesVw> ObtenerVacantes()
        {
            var _repoVacantes = _unidadTrabajo.RepositorioVacantes();
            var vacantes = _repoVacantes.ObtenerRegistros(r => r.EstadoReg == true);
            if (vacantes == null || !vacantes.Any())
                return Enumerable.Empty<VacantesVw>();

            return vacantes.Select(vac => new VacantesVw()
            {
                ClienteID = vac.ClienteID,
                Cliente = vac.Clientes.Nombre,
                Descripcion = vac.Descripcion,
                Nombre = vac.Nombre,
                Estado = vac.Estado,
                FechaRegistro = vac.FechaRegistro,
                VacanteID = vac.VacanteID
            });
        }

        public int RegistrarVacante(VacanteInputModel entrada, out string mensaje)
        {
            mensaje = string.Empty;

            if (entrada == null)
                return (int)ErroresComunes.ValoresNoValido;

            var existeCliente  = _unidadTrabajo.RepositorioClientes()
                                .CantidadRegistros(c => c.ClienteID == entrada.ClienteID) > 0;
            if (!existeCliente)
            {
                mensaje = "Cliente no existe";
                return (int)ErroresComunes.ValoresNoValido;
            }

            if (string.IsNullOrEmpty(entrada.Nombre))
            {
                mensaje = "Debe Completar el Nombre";
                return (int)ErroresComunes.CampoVacio;
            }

            if (string.IsNullOrEmpty(entrada.Descripcion))
            {
                mensaje = "Debe completar la descripcion";
                return (int)ErroresComunes.CampoVacio;
            }

            var vacanteDB = new Vacantes()
            {
                ClienteID = entrada.ClienteID,
                Estado = (int)VacantesEstado.Notificada,
                Nombre = entrada.Nombre,
                Descripcion = entrada.Descripcion,
                FechaRegistro = DateTime.Now,
                EstadoReg = true,
                GuidReg = Guid.NewGuid(),
                FechaModificacion = DateTime.Now
            };
            var _repoVacantes = _unidadTrabajo.RepositorioVacantes();
            _repoVacantes.Insertar(vacanteDB);
            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return vacanteDB.VacanteID;
            return (int)ErroresComunes.ErrorGuardando;


        }

        public VacantesVw ObtenerVacante(int id)
        {
            var _repoVacantes = _unidadTrabajo.RepositorioVacantes();
            var vacante = _repoVacantes.ObtenerRegistro(r => r.EstadoReg == true && r.VacanteID == id);
            if (vacante == null)
                return null;

            var vacanteAptitudes = vacante.VacantesAptitudes.Where(va => va.EstadoReg == true)
                                            .Select(va => new VacanteAptitudViewModel()
                                            {
                                                Aptitud = va.Aptitudes.Nombre,
                                                AptitudID = va.AptitudID,
                                                NivelManejo = va.NivelManejo,
                                                Vacante = va.Vacantes.Nombre,
                                                VacanteID = va.VacanteID,
                                                TiempoExperiencia = va.TiempoExperiencia,
                                                UnidadTiempoExperiencia = va.UnidadTiempoExperiencia,
                                                VacanteAptitudID = va.VacanteAptitudID,
                                            });
            var vacantePerfiles = vacante.VacantesPerfiles.Where(vp => vp.EstadoReg == true)
                                                            .Select(vp => new VacantePerfilViewModel()
                                                            {
                                                                Perfil = vp.Perfiles.Nombre,
                                                                PerfilID = vp.PerfilID,
                                                                Vacante = vp.Vacantes.Nombre,
                                                                VacanteID = vp.VacanteID,
                                                                VacantePerfilID = vp.VacantePerfilID
                                                            });

            return new VacantesVw()
            {
                ClienteID = vacante.ClienteID,
                Cliente = vacante.Clientes.Nombre,
                Descripcion = vacante.Descripcion,
                Nombre = vacante.Nombre,
                Estado = vacante.Estado,
                FechaRegistro = vacante.FechaRegistro,
                VacanteID = vacante.VacanteID,
                Aptitudes = vacanteAptitudes,
                Perfiles = vacantePerfiles
            };
        }

        public int ModificarVacante(VacanteInputModel entrada, out string mensaje)
        {
            mensaje = string.Empty;

            if (entrada == null)
                return (int)ErroresComunes.ValoresNoValido;

            var existeCliente = _unidadTrabajo.RepositorioClientes()
                                .CantidadRegistros(c => c.ClienteID == entrada.ClienteID) > 0;
            if (!existeCliente)
            {
                mensaje = "Cliente no existe";
                return (int)ErroresComunes.ValoresNoValido;
            }

            if (string.IsNullOrEmpty(entrada.Nombre))
            {
                mensaje = "Debe Completar el Nombre";
                return (int)ErroresComunes.CampoVacio;
            }

            if (string.IsNullOrEmpty(entrada.Descripcion))
            {
                mensaje = "Debe completar la descripcion";
                return (int)ErroresComunes.CampoVacio;
            }
            var _repoVacantes = _unidadTrabajo.RepositorioVacantes();
            var vacanteDB = _repoVacantes.ObtenerRegistro(v => v.VacanteID == entrada.VacanteID);
            if (vacanteDB == null)
            {
                mensaje = "Identificador de vacante incorrecto";
                return (int)ErroresComunes.CampoInvalidado;
            }

            if (vacanteDB.ClienteID != entrada.ClienteID)
            {
                if (vacanteDB.Estado == (int)VacantesEstado.Notificada)
                    vacanteDB.ClienteID = entrada.ClienteID;
                else
                {
                    mensaje = "No se puede cambiar el cliente si la vacante ya ha iniciado su proceso";
                    return (int)ErroresComunes.ValoresNoValido;
                }
            }

            vacanteDB.Nombre = entrada.Nombre;
            vacanteDB.Descripcion = entrada.Descripcion;
            vacanteDB.FechaModificacion = DateTime.Now;

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return vacanteDB.VacanteID;
            return (int)ErroresComunes.ErrorGuardando;
        }

        public int RemoverVacante(int vacanteId, out string mensaje)
        {
            var _repoVacantes = _unidadTrabajo.RepositorioVacantes();
            mensaje = string.Empty;
            var vacanteDB = _repoVacantes.ObtenerRegistro(v => v.VacanteID == vacanteId);
            if (vacanteDB == null)
            {
                mensaje = "Identificador de vacante incorrecto";
                return (int)ErroresComunes.CampoInvalidado;
            }

            if (vacanteDB.Estado != (int)VacantesEstado.Notificada)
            {
                mensaje = "No se puede remover una vacante si la vacante ya ha iniciado su proceso, solo puede ser cancelada";
                return (int)ErroresComunes.ValoresNoValido;
            }

            vacanteDB.VacantesAptitudes.ToList().ForEach(vap => vap.EstadoReg = false);
            vacanteDB.VacantesPerfiles.ToList().ForEach(vap => vap.EstadoReg = false);
            vacanteDB.VacantePriorizacion.ToList().ForEach(vp => vp.EstadoReg = false);
            vacanteDB.EstadoReg = false;

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return vacanteDB.VacanteID;
            return (int)ErroresComunes.ErrorGuardando;
        }

        public int CancelarVacante(int vacanteID, out string mensaje)
        {
            var _repoVacantes = _unidadTrabajo.RepositorioVacantes();
            mensaje = string.Empty;
            var vacanteDB = _repoVacantes.ObtenerRegistro(v => v.VacanteID == vacanteID);
            if (vacanteDB != null)
            {
                mensaje = "Identificador de vacante incorrecto";
                return (int)ErroresComunes.CampoInvalidado;
            }

            vacanteDB.Estado = (int)VacantesEstado.Cancelada;
            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return vacanteDB.VacanteID;
            return (int)ErroresComunes.ErrorGuardando;

        }
        #endregion
        #region CRUD-VacanteAptitud
        public VacanteAptitudViewModel ObtenerVacanteAptitud(int vacanteAptitudID)
        {
            var _repoBaseVacantesAptitudes = _unidadTrabajo.RepositorioVacantesAptitudes();
            var vacanteAptitud = _repoBaseVacantesAptitudes.ObtenerRegistro(r => r.EstadoReg == true 
                                                            && r.VacanteAptitudID == vacanteAptitudID);
            if (vacanteAptitud == null)
                return null;

            return new VacanteAptitudViewModel()
            {
                AptitudID = vacanteAptitud.AptitudID,
                Aptitud = vacanteAptitud.Aptitudes == null? string.Empty : vacanteAptitud.Aptitudes.Nombre,
                NivelManejo = vacanteAptitud.NivelManejo,
                TiempoExperiencia = vacanteAptitud.TiempoExperiencia,
                UnidadTiempoExperiencia = vacanteAptitud.UnidadTiempoExperiencia,
                VacanteID = vacanteAptitud.VacanteID,
                Vacante = vacanteAptitud.Vacantes == null? string.Empty : vacanteAptitud.Vacantes.Nombre,
                VacanteAptitudID = vacanteAptitud.VacanteAptitudID
            };
        }

        public int AgregarVacanteAptitud(VacanteAptitudInputModel entrada, out string mensaje)
        {
            mensaje = string.Empty;
            if (entrada == null)
                return (int)ErroresComunes.ValoresNoValido;

            var esAptitudValida = _unidadTrabajo.RepositorioAptitudes()
                                        .CantidadRegistros(c => c.EstadoReg == true && c.AptitudID == entrada.AptitudID) > 0;
            if (!esAptitudValida)
            {
                mensaje = "No se encontro la aptitud elegida";
                return (int)ErroresComunes.ValoresNoValido;
            }
            var _repoVacantes = _unidadTrabajo.RepositorioVacantes();
            var vacanteDB = _repoVacantes.ObtenerRegistro(v => v.EstadoReg == true && v.VacanteID == entrada.VacanteID);
            if (vacanteDB == null)
            {
                mensaje = "No se encontra la vacante elegida";
                return (int)ErroresComunes.ValoresNoEncontrado;
            }

            if (vacanteDB.Estado == (int)VacantesEstado.Cancelada || vacanteDB.Estado == (int)VacantesEstado.Concluida)
            {
                mensaje = "Solo se puede agregar aptitudes a vacantes activas(en estado notificada o proceso)";
                return (int)ErroresComunes.ValoresNoValido;
            }

            if (vacanteDB.VacantesAptitudes != null &&
                vacanteDB.VacantesAptitudes.Any(va => va.EstadoReg == true && va.AptitudID == entrada.AptitudID))
            {
                mensaje = "Actualmente existe una aptitud en la sección sin perfil";
                return (int)ErroresComunes.Duplicidad;
            }

            if (entrada.TiempoExperiencia < 1)
            {
                mensaje = "El tiempo de experiencia debe ser mayor a 1";
                return (int)ErroresComunes.CampoInvalidado;
            }
            var vacanteAptitudDB = new VacantesAptitudes()
            {
                AptitudID = entrada.AptitudID,
                NivelManejo = entrada.NivelManejo,
                TiempoExperiencia = entrada.TiempoExperiencia,
                UnidadTiempoExperiencia = entrada.UnidadTiempoExperiencia,
                VacanteID = vacanteDB.VacanteID,
                EstadoReg = true,
                FechaModificacion = DateTime.Now,
                GuidReg = Guid.NewGuid()
            };
            vacanteDB.VacantesAptitudes.Add(vacanteAptitudDB);

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return vacanteAptitudDB.VacanteAptitudID;

            return (int)ErroresComunes.ErrorGuardando;
        }

        public int ModificarVacanteAptitud(VacanteAptitudInputModel entrada, out string mensaje)
        {
            mensaje = string.Empty;
            if (entrada == null)
                return (int)ErroresComunes.ValoresNoValido;

            var _repoBaseVacantesAptitudes = _unidadTrabajo.RepositorioVacantesAptitudes();
            var vacanteAptitudDB = _repoBaseVacantesAptitudes.ObtenerRegistro(va => va.EstadoReg 
                                                                && va.VacanteAptitudID == entrada.VacanteAptitudID);

            if (vacanteAptitudDB == null)
            {
                mensaje = "No se encontra la aptitud asociada este perfil";
                return (int)ErroresComunes.ValoresNoEncontrado;
            }

            var esAptitudValida = _unidadTrabajo.RepositorioAptitudes()
                                        .CantidadRegistros(c => c.EstadoReg == true && c.AptitudID == entrada.AptitudID) > 0;
            if (!esAptitudValida)
            {
                mensaje = "No se encontro la aptitud elegida";
                return (int)ErroresComunes.ValoresNoEncontrado;
            }
            var _repoVacantes = _unidadTrabajo.RepositorioVacantes();
            var vacanteDB = _repoVacantes.ObtenerRegistro(v => v.EstadoReg == true && v.VacanteID == entrada.VacanteID);
            if (vacanteDB == null)
            {
                mensaje = "No se encontra la vacante elegida";
                return (int)ErroresComunes.ValoresNoEncontrado;
            }

            if (vacanteDB.Estado == (int)VacantesEstado.Cancelada || vacanteDB.Estado == (int)VacantesEstado.Concluida)
            {
                mensaje = "Solo se puede agregar aptitudes a vacantes activas(en estado notificada o proceso)";
                return (int)ErroresComunes.ValoresNoValido;
            }

            if (vacanteDB.VacantesAptitudes != null &&
                vacanteDB.VacantesAptitudes.Any(va => va.EstadoReg == true 
                                            && va.VacanteAptitudID != entrada.VacanteAptitudID
                                            && va.AptitudID == entrada.AptitudID))
            {
                mensaje = "Actualmente existe una aptitud en la sección sin perfil";
                return (int)ErroresComunes.Duplicidad;
            }

            if (entrada.TiempoExperiencia < 1)
            {
                mensaje = "El tiempo de experiencia debe ser mayor a 1";
                return (int)ErroresComunes.CampoInvalidado;
            }

            vacanteAptitudDB.AptitudID = entrada.AptitudID;
            vacanteAptitudDB.NivelManejo = entrada.NivelManejo;
            vacanteAptitudDB.TiempoExperiencia = entrada.TiempoExperiencia;
            vacanteAptitudDB.UnidadTiempoExperiencia = entrada.UnidadTiempoExperiencia;
            vacanteAptitudDB.VacanteID = vacanteDB.VacanteID;
            vacanteAptitudDB.FechaModificacion = DateTime.Now;

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return vacanteAptitudDB.VacanteAptitudID;

            return (int)ErroresComunes.ErrorGuardando;
        }

        public int RemoverVacanteAptitud(int vacanteAptitudID, out string mensaje)
        {
            mensaje = string.Empty;
            var _repoBaseVacantesAptitudes = _unidadTrabajo.RepositorioVacantesAptitudes();
            var vacanteAptitud = _repoBaseVacantesAptitudes.ObtenerRegistro(va => va.EstadoReg &&  va.VacanteAptitudID == vacanteAptitudID);
            if (vacanteAptitud == null)
                return (int)ErroresComunes.ValoresNoEncontrado;

            var vacanteDB = vacanteAptitud.Vacantes;
            if (vacanteDB != null && (vacanteDB.Estado == (int)VacantesEstado.Cancelada || vacanteDB.Estado == (int)VacantesEstado.Concluida))
            {
                mensaje = "Solo se puede eliminar aptitud a vacantes activas(en estado notificada o proceso)";
                return (int)ErroresComunes.ValoresNoValido;
            }

            vacanteAptitud.EstadoReg = false;
            vacanteAptitud.FechaModificacion = DateTime.Now;
            vacanteDB.VacantePriorizacion.Where(v => v.PerfilAptitudID.HasValue == false)
                                        .ToList().ForEach(vp => vp.EstadoReg = false);

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return vacanteAptitudID;
            return (int)ErroresComunes.ErrorGuardando;
                
        }

        #endregion
        #region CRUD-VacantePerfil
        public VacantePerfilViewModel ObtenerVacantePerfil(int vacantePerfilID)
        {
            var _repoBaseVacantesPerfiles = _unidadTrabajo.RepositorioVacantesPerfiles();
            var vacantePerfil = _repoBaseVacantesPerfiles.ObtenerRegistro(r => r.EstadoReg == true
                                                            && r.VacantePerfilID == vacantePerfilID);
            if (vacantePerfil == null)
                return null;

            return new VacantePerfilViewModel()
            {
                PerfilID = vacantePerfil.PerfilID,
                Perfil = vacantePerfil.Perfiles == null ? string.Empty: vacantePerfil.Perfiles.Nombre,
                VacanteID = vacantePerfil.VacanteID,
                Vacante = vacantePerfil.Vacantes == null? string.Empty: vacantePerfil.Vacantes.Nombre,
                VacantePerfilID = vacantePerfil.VacantePerfilID,
            };
        }

        public int AgregarVacantePerfil(VacantePerfilInputModel entrada, out string mensaje)
        {
            mensaje = string.Empty;
            if (entrada == null)
                return (int)ErroresComunes.ValoresNoValido;

            var esPerfilValido = _unidadTrabajo.RepositorioPerfiles()
                                        .CantidadRegistros(c => c.EstadoReg == true && c.PerfilID == entrada.PerfilID) > 0;
            if (!esPerfilValido)
            {
                mensaje = "No se encontro el perfil elegido";
                return (int)ErroresComunes.ValoresNoValido;
            }
            var _repoVacantes = _unidadTrabajo.RepositorioVacantes();
            var vacanteDB = _repoVacantes.ObtenerRegistro(v => v.EstadoReg == true && v.VacanteID == entrada.VacanteID);
            if (vacanteDB == null)
            {
                mensaje = "No se encontra la vacante elegida";
                return (int)ErroresComunes.ValoresNoEncontrado;
            }

            if (vacanteDB.Estado == (int)VacantesEstado.Cancelada || vacanteDB.Estado == (int)VacantesEstado.Concluida)
            {
                mensaje = "Solo se puede agregar perfiles a vacantes activas(en estado notificada o proceso)";
                return (int)ErroresComunes.ValoresNoValido;
            }

            if (vacanteDB.VacantesPerfiles != null &&
                vacanteDB.VacantesPerfiles.Any(va => va.EstadoReg == true && va.PerfilID == entrada.PerfilID))
            {
                mensaje = "Actualmente existe este perfil";
                return (int)ErroresComunes.Duplicidad;
            }


            var vacantePerfilesDB = new VacantesPerfiles()
            {
                PerfilID = entrada.PerfilID,
                VacanteID = vacanteDB.VacanteID,
                EstadoReg = true,
                FechaModificacion = DateTime.Now,
                GuidReg = Guid.NewGuid()
            };
            vacanteDB.VacantesPerfiles.Add(vacantePerfilesDB);

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return vacantePerfilesDB.VacantePerfilID;

            return (int)ErroresComunes.ErrorGuardando;
        }

        public int ModificarVacantePerfil(VacantePerfilInputModel entrada, out string mensaje)
        {
            mensaje = string.Empty;
            if (entrada == null)
                return (int)ErroresComunes.ValoresNoValido;

            var _repoBaseVacantesPerfiles = _unidadTrabajo.RepositorioVacantesPerfiles();
            var vacantePerfilDB = _repoBaseVacantesPerfiles.ObtenerRegistro(vp => vp.EstadoReg == true 
                                                        && vp.VacantePerfilID == entrada.VacantePerfilID);

            if (vacantePerfilDB == null)
            {
                mensaje = "No se ha encontrado el perfil asociado a esta vacante";
                return (int)ErroresComunes.ValoresNoEncontrado;
            }

            var esPerfilValido = _unidadTrabajo.RepositorioPerfiles()
                                        .CantidadRegistros(c => c.EstadoReg == true && c.PerfilID == entrada.PerfilID) > 0;
            if (!esPerfilValido)
            {
                mensaje = "No se encontro el perfil elegido";
                return (int)ErroresComunes.ValoresNoValido;
            }
            var _repoVacantes = _unidadTrabajo.RepositorioVacantes();
            var vacanteDB = _repoVacantes.ObtenerRegistro(v => v.EstadoReg == true && v.VacanteID == entrada.VacanteID);
            if (vacanteDB == null)
            {
                mensaje = "No se encontra la vacante elegida";
                return (int)ErroresComunes.ValoresNoEncontrado;
            }

            if (vacanteDB.Estado == (int)VacantesEstado.Cancelada || vacanteDB.Estado == (int)VacantesEstado.Concluida)
            {
                mensaje = "Solo se puede agregar perfiles a vacantes activas(en estado notificada o proceso)";
                return (int)ErroresComunes.ValoresNoValido;
            }

            if (vacanteDB.VacantesPerfiles != null &&
                vacanteDB.VacantesPerfiles.Any(va => va.EstadoReg == true && va.VacantePerfilID != entrada.VacantePerfilID
                                            && va.PerfilID == entrada.PerfilID))
            {
                mensaje = "Actualmente existe este perfil";
                return (int)ErroresComunes.Duplicidad;
            }

            vacantePerfilDB.PerfilID = entrada.PerfilID;
            vacantePerfilDB.VacanteID = vacanteDB.VacanteID;
            vacantePerfilDB.FechaModificacion = DateTime.Now;

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return vacantePerfilDB.VacantePerfilID;

            return (int)ErroresComunes.ErrorGuardando;
        }

        public int RemoverVacantePerfil(int vacantePerfilID, out string mensaje)
        {
            var _repoBaseVacantesPerfiles = _unidadTrabajo.RepositorioVacantesPerfiles();
            mensaje = string.Empty;
            var vacantePerfil = _repoBaseVacantesPerfiles.ObtenerRegistro(vp => vp.EstadoReg && vp.VacantePerfilID == vacantePerfilID);
            var vacanteDB = vacantePerfil.Vacantes; 
            if (vacanteDB != null && (vacanteDB.Estado == (int)VacantesEstado.Cancelada || vacanteDB.Estado == (int)VacantesEstado.Concluida))
            {
                mensaje = "Solo se puede remover perfiles a vacantes activas(en estado notificada o proceso)";
                return (int)ErroresComunes.ValoresNoValido;
            }

            vacantePerfil.EstadoReg = false;
            vacantePerfil.FechaModificacion = DateTime.Now;
            vacantePerfil.Perfiles.PerfilesAptitudes.ToList()
                    .ForEach(pa => pa.VacantePriorizacion.ToList().ForEach(vc => vc.EstadoReg = false));

            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return vacantePerfilID;

            return (int)ErroresComunes.ErrorGuardando;

        }
        #endregion




        #region Priorizacion
        public IEnumerable<VacantePriorizacionViewModel> ObtenerAptitudesPriorizacion(int vacante)
        {
            var _repoVacantes = _unidadTrabajo.RepositorioVacantes();
            var resultados = _repoVacantes.ObtenerAptitudesVacante<VacantePriorizacionViewModel>(vacante);
            if (resultados == null || !resultados.Any())
                return Enumerable.Empty<VacantePriorizacionViewModel>();

            return resultados;
        }
        #endregion


        public int AgregarPriorizacion(VacantePriorizacionInputModel entrada)
        {
            if (entrada == null)
                return (int)ErroresComunes.CampoInvalidado;
            
            var _repoVacantes = _unidadTrabajo.RepositorioVacantes();
            var vacanteDB = _repoVacantes.ObtenerRegistro(v => v.VacanteID == entrada.VacanteID);
            if (vacanteDB == null)
                return (int)ErroresComunes.CampoInvalidado;

            var priorizacion = new VacantePriorizacion()
            {
                EstadoReg = true,
                Guid = Guid.NewGuid(),
                FechaModificacion = DateTime.Now,
                AptitudID = entrada.AptitudID,
                Orden = ObtenerProximoOrden(entrada.VacanteID),
                PerfilAptitudID = entrada.AptitudPerfilID,
                VacanteID = entrada.VacanteID,
                ValoracionMinimaAptitud = entrada.ValoracionAptitud
            };

            vacanteDB.VacantePriorizacion.Add(priorizacion);
            var seGuardoCambios = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambios)
                return priorizacion.VacantePriorizacionID;
            else
                return (int)ErroresComunes.ErrorGuardando;


        }

        private int ObtenerProximoOrden(int vacante)
        {
            var _repoVacantes = _unidadTrabajo.RepositorioVacantes();
            var orden = _repoVacantes.ConsultarProximoNumeroOrden(vacante);
            if (orden == 0)
            {
                orden = 1;
            }
            else
            {
                orden += 1;
            }
            return orden;
        }


        public int RemoverPriorizacion(int priorizacionID)
        {
            var _repoBaseVacantesPriorizacion = _unidadTrabajo.RepositorioVacantesPriorizacion();
            var priorizacion = _repoBaseVacantesPriorizacion.ObtenerRegistro(p => p.VacantePriorizacionID == priorizacionID);
            if (priorizacion == null)
                return (int)ErroresComunes.ValoresNoEncontrado;

            priorizacion.EstadoReg = false;
            
            return AlterarOrden(priorizacion.VacanteID);
        }


        public VacantePriorizacionViewModel ObtenerAptitudPriorizacion(int priorizacionID)
        {
            var _repoBaseVacantesPriorizacion = _unidadTrabajo.RepositorioVacantesPriorizacion();
            var priorizacion = _repoBaseVacantesPriorizacion.ObtenerRegistro(p => p.VacantePriorizacionID == priorizacionID);
            if (priorizacion == null)
                return null;

            return new VacantePriorizacionViewModel()
            {
                Aptitud = priorizacion.Aptitudes.Nombre,
                AptitudID = priorizacion.AptitudID,
                AptitudPerfilID = priorizacion.PerfilAptitudID,
                ValoracionMinimaAptitud = priorizacion.ValoracionMinimaAptitud
            };
        }


        public int ModificarPriorizacion(int priorizacionID, int valoracionAptitud)
        {
            var _repoBaseVacantesPriorizacion = _unidadTrabajo.RepositorioVacantesPriorizacion();
            var priorizacion = _repoBaseVacantesPriorizacion.ObtenerRegistro(p => p.VacantePriorizacionID == priorizacionID);
            if (priorizacion == null)
                return (int)ErroresComunes.ValoresNoEncontrado;

            priorizacion.ValoracionMinimaAptitud = valoracionAptitud;

            var seGuardoCambio = _unidadTrabajo.GuardarCambios();
            if (seGuardoCambio)
                return priorizacion.VacantePriorizacionID;
            return (int)ErroresComunes.ErrorGuardando;
        }

        public int AlterarOrden(int vacanteID)
        {
            var _repoBaseVacantesPriorizacion = _unidadTrabajo.RepositorioVacantesPriorizacion();
            var priorizaciones = _repoBaseVacantesPriorizacion.ObtenerRegistros(vp => vp.EstadoReg == true && vp.VacanteID == vacanteID)
                                .OrderBy(v => v.Orden).ToList();
            if (priorizaciones == null)
                return (int)ErroresComunes.CampoInvalidado;

            var orden = 1;
            foreach (var priorizacion in priorizaciones)
            {
                priorizacion.Orden = orden++;
            }

            var seGuardaCambios = _unidadTrabajo.GuardarCambios();
            if (!seGuardaCambios)
                return (int)ErroresComunes.ErrorGuardando;
            return vacanteID;
        }


        public int AlterarOrden(int vacanteID, int[] ordenAplicado)
        {
            var _repoBaseVacantesPriorizacion = _unidadTrabajo.RepositorioVacantesPriorizacion();
            var priorizaciones = _repoBaseVacantesPriorizacion.ObtenerRegistros(vp => vp.EstadoReg == true && vp.VacanteID == vacanteID)
                                                                .ToList();
            if (priorizaciones == null)
                return (int)ErroresComunes.CampoInvalidado;

            var orden = 1;
            foreach (var priorizacionID in ordenAplicado)
            {
                var priorizacion = priorizaciones.FirstOrDefault(f => f.VacantePriorizacionID == priorizacionID);
                if (priorizacion != null)
                    priorizacion.Orden = orden++;
            }

            var seGuardaCambios = _unidadTrabajo.GuardarCambios();
            if (!seGuardaCambios)
                return (int) ErroresComunes.ErrorGuardando;
            return vacanteID;
        }
    }
}
