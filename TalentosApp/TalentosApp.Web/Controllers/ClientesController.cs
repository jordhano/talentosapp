﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using TalentosApp.Contractos.Servicios;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.Enumeradores;
using TalentosApp.Enumeradores.Exts;

namespace TalentosApp.Web.Controllers
{
    public class ClientesController : Controller
    {
        private readonly IServicioClientes _servicioClientes;
        public ClientesController(IServicioClientes servicioClientes)
        {
            _servicioClientes = servicioClientes;
        }

        #region ViewData
        public ActionResult ListadoClientesPaginado(int? pagina, int registroPorPagina = 5)
        {
            var clientes = _servicioClientes.ObtenerClientes().ToPagedList(pagina??1, registroPorPagina);
            return View(clientes);
        }

        public ActionResult DetalleCliente(int? id)
        {
            if (!id.HasValue)
                return RedirectToAction("MyError","Home",new {id = -2});
            var cliente = _servicioClientes.ObtenerCliente(id.Value);
            if (cliente == null)
                return RedirectToAction("MyError", "Home", new {id = -2});

            return View(cliente);
        }

        public ActionResult RegistrarCliente() 
        {
            return View();
        }

        #endregion

        #region POSTAjax
        [HttpPost]
        public JsonResult RegistrarCliente(ClienteInputModel entrada)
        {
            var resultado = _servicioClientes.RegistrarCliente(entrada);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });
            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString()});
        }

        [HttpPost]
        public ActionResult ModificarCliente(ClienteInputModel entrada)
        {
            var resultado = _servicioClientes.ModificarCliente(entrada);
            if (resultado > 0)
                return Json(new { error = false , resultado = resultado});
            return Json(new { error = false, resultado = ((ErroresComunes)resultado).GetDescriptionString() });
        }

        [HttpPost]
        public ActionResult EliminarCliente(int clienteID)
        {
            var resultado = _servicioClientes.RemoverCliente(clienteID);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });
            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString() });
        }

        #endregion
    }
}