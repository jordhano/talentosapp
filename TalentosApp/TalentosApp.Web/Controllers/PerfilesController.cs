﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using TalentosApp.Contractos.Servicios;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.Enumeradores;
using TalentosApp.Enumeradores.Exts;

namespace TalentosApp.Web.Controllers
{
    public class PerfilesController : Controller
    {
        private readonly IServicioPerfiles _servicioPerfiles;
        private readonly IServicioAptitudes _servicioAptitudes;
        public PerfilesController(IServicioPerfiles servicioPerfiles,IServicioAptitudes servicioAptitudes)
        {
            _servicioPerfiles = servicioPerfiles;
            _servicioAptitudes = servicioAptitudes;

        }
        #region VIEW

        public ActionResult RegistrarPerfil()
        {
            return View();
        }

        public ActionResult ListadoPerfilesPaginado(int? pagina, int registroPorPagina = 5)
        {
            var perfiles = _servicioPerfiles.ObtenerPerfiles().ToPagedList(pagina ?? 1, registroPorPagina);
            return View(perfiles);
        }

        public ActionResult DetallePerfil(int? id)
        {
            if (!id.HasValue)
                return RedirectToAction("MyError", "Home", new { id = -2 });

            var perfil = _servicioPerfiles.ObtenerPerfil(id.Value);
            if (perfil == null)
                return RedirectToAction("MyError", "Home", new { id = -2 });

            return View(perfil);
        }

        #endregion




        #region POSTDATA

        [HttpPost]
        public JsonResult RegistrarPerfil(PerfilInputModel entrada)
        {
            var resultado = _servicioPerfiles.RegistrarPerfil(entrada);
            if (resultado > 0)
                return Json(new {
                    error = false,
                    resultado = resultado
                },JsonRequestBehavior.DenyGet);

            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString() });
        }

        #endregion

        #region AJAXPOST
        [HttpPost]
        public JsonResult ModificarPerfil(PerfilInputModel entrada)
        {
            var resultado = _servicioPerfiles.ModificarPerfil(entrada);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });

            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString() });
        }

        [HttpPost]
        public JsonResult RemoverPerfil(int perfilID)
        {
            var resultado = _servicioPerfiles.RemoverPerfil(perfilID);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });

            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString() });
        }

        [HttpPost]
        public JsonResult AgregarAptitudPerfil(PerfilAptitudInputModel entrada)
        {
            var resultado = _servicioPerfiles.AgregarAptitudPerfil(entrada);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });
            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString()});
        }

        [HttpPost]
        public JsonResult ModificarAptitudPerfil(PerfilAptitudInputModel entrada)
        {
            var resultado = _servicioPerfiles.ModificarAptitudPerfil(entrada);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });
            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString()});
        }

        [HttpPost]
        public JsonResult RemoverAptitudPerfil(int aptitudPerfilID)
        {
            var resultado = _servicioPerfiles.RemoverAptitudPerfil(aptitudPerfilID);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });
            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString()});
        }

        #endregion



        #region AJAXResults
        [HttpGet]
        public JsonResult ObtenerPerfil(int id)
        {

            var perfil = _servicioPerfiles.ObtenerPerfil(id);
            if (perfil == null)
                return Json(new {error = true, resultado = "Perfil no encontrado"},JsonRequestBehavior.AllowGet);

            return Json(new { error = false, resultado = perfil }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ObtenerPerfilAptitud(int perfilAptitudID)
        {
            var resultado = _servicioPerfiles.ObtenerPerfilAptitud(perfilAptitudID);
            if (resultado != null)
                return Json(new { error = false, resultado = resultado },JsonRequestBehavior.AllowGet);
            return Json(new { error = true, 
                            resultado = (ErroresComunes.ValoresNoEncontrado).GetDescriptionString(),
                        }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult ListadoAptitudesDisponiblePerfil(int perfilID)
        {
            var aptitudes = _servicioAptitudes.ObtenerAptitudesDisponiblePerfil(perfilID);
            var resultado = aptitudes.Select(a => new { id = a.AptitudID, name = a.Nombre });
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        

        #endregion
    }
}