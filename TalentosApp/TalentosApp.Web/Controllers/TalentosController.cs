﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.Contractos.Servicios;
using TalentosApp.Enumeradores;
using TalentosApp.Enumeradores.Exts;

namespace TalentosApp.Web.Controllers
{
    public class TalentosController : Controller
    {
        private readonly IServicioTalentos _servicioTalentos;
        private readonly IServicioAptitudes _servicioAptitudes;
        public TalentosController(IServicioTalentos servicioTalentos,
            IServicioAptitudes servicioAptitudes)
        {
            _servicioTalentos = servicioTalentos;
            _servicioAptitudes = servicioAptitudes;
        }
        #region ViewData
        public ActionResult ListadoTalentosPaginado(int? pagina, int registroPorPagina = 5)
        {
            var talentos = _servicioTalentos.ObtenerTalentos().ToPagedList(pagina ?? 1, registroPorPagina);
            return View(talentos);
        }

        public ActionResult RegistrarTalento()
        {
            return View();
        }

        public ActionResult DetalleTalento(int? id)
        {
            if (!id.HasValue)
                return RedirectToAction("MyError", "Home", new { id = -2 });

            var resultado = _servicioTalentos.ObtenerTalento(id.Value);
            if (resultado == null)
                return RedirectToAction("MyError", "Home", new { id = -2 });
            return View(resultado);
        }
        
        #endregion

        #region PostAJAX
        [HttpPost]
        public JsonResult RegistrarTalento(TalentoInputModel entrada)
        {
            var resultado = _servicioTalentos.RegistrarTalentos(entrada);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });
            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString() });
        }

        [HttpPost]
        public JsonResult ModificarTalento(TalentoInputModel entrada)
        {
            var resultado = _servicioTalentos.ModificarTalentos(entrada);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });
            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString() });

        }

        [HttpPost]
        public JsonResult RemoverTalento(int talentoID)
        {
            var resultado = _servicioTalentos.EliminarTalento(talentoID);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });
            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString() });
        }

        [HttpPost]
        public JsonResult AgregarTalentoAptitud(TalentoAptitudInputModel entrada)
        {
            var resultado = _servicioTalentos.AgregarTalentoAptitud(entrada);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });
            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString() });
        }
        [HttpPost]
        public JsonResult ModificarTalentoAptitud(TalentoAptitudInputModel entrada)
        {
            var resultado = _servicioTalentos.ModificarTalentoAptitud(entrada);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });
            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString() });
        }
        [HttpPost]
        public JsonResult RemoverTalentoAptitud(int talentoAptitudID)
        {
            var resultado = _servicioTalentos.RemoverTalentoApitutd(talentoAptitudID);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });
            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString() });
        }

        #endregion

        #region GetAJAX
        [HttpGet]
        public JsonResult ListadoAptitudesDisponibleTalentos(int talentoID)
        {
            var aptitudes = _servicioAptitudes.ObtenerAptitudesDisponibleTalentos(talentoID);
            var resultado = aptitudes.Select(a => new { id = a.AptitudID, name = a.Nombre });
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ObtenerTalentoAptitud(int talentoAptitudID)
        {
            var resultado = _servicioTalentos.ObtenerTalentoAptitud(talentoAptitudID);
            if (resultado != null)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.AllowGet);
            return Json(new
            {
                error = true,
                resultado = (ErroresComunes.ValoresNoEncontrado).GetDescriptionString(),
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}