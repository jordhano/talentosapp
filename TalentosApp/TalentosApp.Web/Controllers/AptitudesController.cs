﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using TalentosApp.Contractos.Servicios;
using TalentosApp.Enumeradores;
using TalentosApp.Enumeradores.Exts;
using TalentosApp.TransferenciaDatos.Entradas;

namespace TalentosApp.Web.Controllers
{
    public class AptitudesController : Controller
    {
        private readonly IServicioAptitudes _servicioAptitudes;
        public AptitudesController(IServicioAptitudes servicioAptitudes)
        {
            _servicioAptitudes = servicioAptitudes;
        }

        #region ViewData

        public ActionResult ListadoAptitudesPaginado(int? pagina, int registroPorPagina = 5)
        {
            var aptitudes = _servicioAptitudes.ObtenerAptitudes().ToPagedList(pagina ?? 1,registroPorPagina);
            return View(aptitudes);
        }

        public ActionResult DetalleAptitud(int? id)
        {
            if (!id.HasValue)
                return RedirectToAction("MyError", "Home", new { id = -2 });

            var aptitud = _servicioAptitudes.ObtenerAptitud(id.Value);
            if (aptitud == null)
                return RedirectToAction("MyError", "Home", new { id = -2 });
            return View(aptitud);
        }
        public ActionResult RegistrarAptitud()
        {
            return View();
        }

        #endregion


        #region AjaxResult

        [HttpGet]
        public JsonResult ObtenerAptitud(int aptitudId)
        {
            var aptitud = _servicioAptitudes.ObtenerAptitud(aptitudId);
            if (aptitud == null)
                return Json(new { error = true, resultado = "Aptitud no encontrada" }, JsonRequestBehavior.AllowGet);
            return Json(new { error = false, resultado = aptitud }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region AjaxPOST

        [HttpPost]
        public JsonResult RemoverAptitud(int aptitudId)
        {
            var resultado = _servicioAptitudes.RemoverAptitud(aptitudId);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });
            return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString()});
        }

        [HttpPost]
        public JsonResult ModificarAptitud(AptitudInputModel entrada)
        {
            var resultado = _servicioAptitudes.ModificarAptitud(entrada);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado });
            return Json(new { error = false, resultado = ((ErroresComunes)resultado).GetDescriptionString() });
        }

        [HttpPost]
        public JsonResult RegistrarAptitud(AptitudInputModel entrada)
        {
            try
            {
                var resultado = _servicioAptitudes.RegistrarAptitud(entrada);

                if (resultado > 0)
                    return Json(new { error = false, resultado = resultado });

                return Json(new { error = true, resultado = ((ErroresComunes)resultado).GetDescriptionString() });

            }
            catch (Exception e)
            {
                return Json(new { error = true, resultado = "Solicitud no procesada" });
            }
        }

        #endregion
    }
}
