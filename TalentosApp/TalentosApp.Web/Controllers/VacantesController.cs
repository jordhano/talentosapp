﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using TalentosApp.Contractos.Servicios;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.Enumeradores;
using TalentosApp.Enumeradores.Exts;
using TalentosApp.TransferenciaDatos.VistaModelos;

namespace TalentosApp.Web.Controllers
{
    public class VacantesController : Controller
    {
        private readonly IServicioVacantes _servicioVacantes;
        private readonly IServicioAptitudes _servicioAptitudes;
        private readonly IServicioPerfiles _servicioPerfiles;
        private readonly IServicioClientes _servicioClientes;
        public VacantesController(IServicioVacantes servicioVacantes,
            IServicioAptitudes servicioAptitudes,IServicioPerfiles servicioPerfiles,
            IServicioClientes servicioClientes)
        {
            _servicioVacantes = servicioVacantes;
            _servicioAptitudes = servicioAptitudes;
            _servicioPerfiles = servicioPerfiles;
            _servicioClientes = servicioClientes;
        }
        #region ViewData

        public ActionResult ListadoVacantesPaginado(int? pagina, int registroPorPagina = 5)
        {
            var clientes = _servicioVacantes.ObtenerVacantes().ToPagedList(pagina ?? 1, registroPorPagina);
            return View(clientes);
        }

        public ActionResult RegistrarVacante()
        {
            ViewBag.Clientes = CargarClientes();
            return View();
        }

        public ActionResult DetalleVacante(int? id)
        {
            if (!id.HasValue)
                return RedirectToAction("MyError", "Home", new { id = -2 });
            var vacante = _servicioVacantes.ObtenerVacante(id.Value);
            if (vacante == null)
                return RedirectToAction("MyError", "Home", new { id = -2 });
            
            ViewBag.Clientes = CargarClientes();
            return View(vacante);
        }
        #endregion

        #region AjaxGET-Todos
        public JsonResult ObtenerVacante(int vacanteID)
        {
            var vacante = _servicioVacantes.ObtenerVacante(vacanteID);
            if (vacante == null)
                return Json(new { error = true, resultado = "Vacante no encontrada" }, JsonRequestBehavior.AllowGet);
            return Json(new { error = false, resultado = vacante }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObtenerVacantePerfil(int vacantePerfilID)
        {
            var vacantePerfil = _servicioVacantes.ObtenerVacantePerfil(vacantePerfilID);
            if (vacantePerfil == null)
                return Json(new { error = true, resultado = "Perfil de la vacante no enontrada " }, JsonRequestBehavior.AllowGet);
            return Json(new { error = false, resultado = vacantePerfil }, JsonRequestBehavior.AllowGet);
        }

        
        public JsonResult ObtenerVacanteAptitud(int vacanteAptitudID)
        {
            var vacanteAptitud = _servicioVacantes.ObtenerVacanteAptitud(vacanteAptitudID);
            if (vacanteAptitud == null)
                return Json(new { error = true, resultado = "Perfil de la vacante no enontrada " }, JsonRequestBehavior.AllowGet);
            return Json(new { error = false, resultado = vacanteAptitud }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ListadoAptitudesDisponibleVacantes(int vacanteID)
        {
            var aptitudes = _servicioAptitudes.ObtenerAptitudesDisponibleVacantes(vacanteID);
            var resultado = aptitudes.Select(a => new { id = a.AptitudID, name = a.Nombre });
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult ListadoPerfilesDisponibleVacante(int vacanteID)
        {
            var perfiles = _servicioPerfiles.ObtenerPerfilesDisponibleVacantes(vacanteID);
            var resultado = perfiles.Select(a => new { id = a.PerfilID, name = a.Nombre });
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ListadoAptitudesPriorizacion(int vacanteID)
        {
            var aptitudes = _servicioVacantes.ObtenerAptitudesPriorizacion(vacanteID);
            var especificadas = aptitudes.Where(a => a.VacantePriorizacionID.HasValue == true).OrderByDescending(v => v.Orden);
            var noEspecificadas = aptitudes.Where(a => a.VacantePriorizacionID.HasValue == false);
            var aptitudesDisponible = noEspecificadas.Select(e => new { id= e.AptitudID.ToString(), name = e.Aptitud });
            return Json(new { 
                                Especificadas = especificadas, 
                                NoEspecificadas = noEspecificadas,
                                AptiudesDisponibles = aptitudesDisponible
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public PartialViewResult VacantePriorizacion(int id)
        {
            var aptitudes = _servicioVacantes.ObtenerAptitudesPriorizacion(id);
            var vm = new PartialVacantePriorizacionViewModel(aptitudes,id);
            return PartialView("_partialVacantePriorizacion",vm);
        }
        public JsonResult ObtenerPriorizacion(int priorizacionID)
        {
            var vacanteAptitud = _servicioVacantes.ObtenerAptitudPriorizacion(priorizacionID);
            if (vacanteAptitud == null)
                return Json(new { error = true, resultado = "Perfil de la vacante no enontrada " }, JsonRequestBehavior.AllowGet);
            return Json(new { error = false, resultado = vacanteAptitud }, JsonRequestBehavior.AllowGet);
        }
        

        #endregion

        #region AjaxPOST-Vacantes
        [HttpPost]
        public JsonResult RegistrarVacante(VacanteInputModel entrada)
        {
            var mensaje = string.Empty;
            var resultado = _servicioVacantes.RegistrarVacante(entrada, out mensaje);
            if (resultado > 0)
                return Json(new {error = false, resultado = resultado}, JsonRequestBehavior.DenyGet);
            
            return Json(new { error = true, resultado = (mensaje != string.Empty) ? 
                                mensaje : ((ErroresComunes) resultado).GetDescriptionString()
                        });
        }
        [HttpPost]
        public JsonResult ModificarVacante(VacanteInputModel entrada)
        {
            var mensaje = string.Empty;
            var resultado = _servicioVacantes.ModificarVacante(entrada, out mensaje);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.DenyGet);

            return Json(new
            {
                error = true,
                resultado = (mensaje != string.Empty) ?
                    mensaje : ((ErroresComunes)resultado).GetDescriptionString()
            });
        }
        [HttpPost]
        public JsonResult EliminarVacante(int vacanteID)
        {
            var mensaje = string.Empty;
            var resultado = _servicioVacantes.RemoverVacante(vacanteID, out mensaje);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.DenyGet);

            return Json(new
            {
                error = true,
                resultado = (mensaje != string.Empty) ?
                    mensaje : ((ErroresComunes)resultado).GetDescriptionString()
            });
        }
        [HttpPost]
        public JsonResult CancelarVacante(int vacanteID)
        {
            var mensaje = string.Empty;
            var resultado = _servicioVacantes.CancelarVacante(vacanteID, out mensaje);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.DenyGet);

            return Json(new
            {
                error = true,
                resultado = (mensaje != string.Empty) ?
                    mensaje : ((ErroresComunes)resultado).GetDescriptionString()
            });
        }
        #endregion 
        
        #region AjaxPOST-Aptitudes
        public JsonResult RegistrarVacanteAptitud(VacanteAptitudInputModel entrada)
        {
            var mensaje = string.Empty;
            var resultado = _servicioVacantes.AgregarVacanteAptitud(entrada, out mensaje);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.DenyGet);

            return Json(new
            {
                error = true,
                resultado = (mensaje != string.Empty) ?
                    mensaje : ((ErroresComunes)resultado).GetDescriptionString()
            });
        }
        public JsonResult ModificarVacanteAptitud(VacanteAptitudInputModel entrada)
        {
            var mensaje = string.Empty;
            var resultado = _servicioVacantes.ModificarVacanteAptitud(entrada,out mensaje);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.DenyGet);

            return Json(new
            {
                error = true,
                resultado = (mensaje != string.Empty) ?
                    mensaje : ((ErroresComunes)resultado).GetDescriptionString()
            });
        }
        public JsonResult RemoverVacanteAptitud(int vacanteAptitudID)
        {
            var mensaje = string.Empty;
            var resultado = _servicioVacantes.RemoverVacanteAptitud(vacanteAptitudID, out mensaje);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.DenyGet);

            return Json(new
            {
                error = true,
                resultado = (mensaje != string.Empty) ?
                    mensaje : ((ErroresComunes)resultado).GetDescriptionString()
            });
        }
        #endregion

        #region AjaxPOST-Perfiles
        public JsonResult RegistrarVacantePerfiles(VacantePerfilInputModel entrada)
        {
            var mensaje = string.Empty;
            var resultado = _servicioVacantes.AgregarVacantePerfil(entrada, out mensaje);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.DenyGet);

            return Json(new
            {
                error = true,
                resultado = (mensaje != string.Empty) ?
                    mensaje : ((ErroresComunes)resultado).GetDescriptionString()
            });
        }
        public JsonResult ModificarVacantePerfiles(VacantePerfilInputModel entrada)
        {
            var mensaje = string.Empty;
            var resultado = _servicioVacantes.ModificarVacantePerfil(entrada, out mensaje);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.DenyGet);

            return Json(new
            {
                error = true,
                resultado = (mensaje != string.Empty) ?
                    mensaje : ((ErroresComunes)resultado).GetDescriptionString()
            });
        }
        public JsonResult RemoverVacantePerfiles(int vacantePerfilID)
        {
            var mensaje = string.Empty;
            var resultado = _servicioVacantes.RemoverVacantePerfil(vacantePerfilID, out mensaje);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.DenyGet);

            return Json(new
            {
                error = true,
                resultado = (mensaje != string.Empty) ?
                    mensaje : ((ErroresComunes)resultado).GetDescriptionString()
            });
        }
        #endregion

        #region AjaxPOST - Priorizacion

        public JsonResult AgregarPriorizacion(VacantePriorizacionInputModel entrada)
        {
            var resultado = _servicioVacantes.AgregarPriorizacion(entrada);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.DenyGet);

            return Json(new
            {
                error = true,
                resultado =  ((ErroresComunes)resultado).GetDescriptionString()
            });
        }

        public JsonResult RemoverPriorizacion(int priorizacionID)
        {
            var resultado = _servicioVacantes.RemoverPriorizacion(priorizacionID);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.DenyGet);

            return Json(new
            {
                error = true,
                resultado = ((ErroresComunes)resultado).GetDescriptionString()
            });
        }

        public JsonResult ModificarPriorizacion(int priorizacionID, int valoracionAptitud)
        {
            var resultado = _servicioVacantes.ModificarPriorizacion(priorizacionID, valoracionAptitud);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.DenyGet);

            return Json(new
            {
                error = true,
                resultado = ((ErroresComunes)resultado).GetDescriptionString()
            });
        }


        public JsonResult AlterarOrden(int vacanteID, int[] ordenAplicado)
        {
            var resultado = _servicioVacantes.AlterarOrden(vacanteID, ordenAplicado);
            if (resultado > 0)
                return Json(new { error = false, resultado = resultado }, JsonRequestBehavior.DenyGet);

            return Json(new
            {
                error = true,
                resultado = ((ErroresComunes)resultado).GetDescriptionString()
            });
        }
        



        //public JsonResult AgregarPriorizacion(int vacanteID)
        //{
        //    var _servicioVacante = _servicioFactoria.ServicioVacantes();
        //    var aptitudes = _servicioVacante.ObtenerAptitudesPriorizacion(vacanteID);
        //    var especificadas = aptitudes.Where(a => a.VacantePriorizacionID.HasValue == true).OrderByDescending(v => v.Orden);
        //    var noEspecificadas = aptitudes.Where(a => a.VacantePriorizacionID.HasValue == false);
        //    var aptitudesDisponible = noEspecificadas.Select(e => new { id = e.AptitudID.ToString(), name = e.Aptitud });
        //    return Json(new
        //    {
        //        Especificadas = especificadas,
        //        NoEspecificadas = noEspecificadas,
        //        AptiudesDisponibles = aptitudesDisponible
        //    }, JsonRequestBehavior.AllowGet);
        //}

        #endregion

        #region LoadData
        private List<SelectListItem> CargarClientes()
        {
            var clientes = _servicioClientes.ObtenerClientes();
            return clientes.Select(c => new SelectListItem()
                    {
                        Text = c.Nombre,
                        Value = c.ClienteID.ToString()
                    }).ToList();
        }
        #endregion
    }
}