﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TalentosApp.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult MyError(int? id = null)
        {
            var mensaje = "Pagina no disponible";
            if (id.HasValue)
                switch (id.Value)
                {
                    case -1:
                        mensaje = "No posee permiso para esta página";
                        break;
                    case -2:
                        mensaje = "Registro no encontrado";
                        break;
                    case 1:
                        mensaje = "Opción aun no realizada";
                        break;
                    default:
                        break;
                }
            return View(model:mensaje);
        }
    }
}