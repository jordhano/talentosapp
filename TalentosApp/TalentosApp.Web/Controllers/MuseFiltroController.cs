﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TalentosApp.Contractos.Servicios;
using TalentosApp.Enumeradores;
using TalentosApp.TransferenciaDatos.Entradas;

namespace TalentosApp.Web.Controllers
{
    public class MuseFiltroController : Controller
    {
        private readonly IServicioMuseFiltro _servicioFiltroMuse;
        private readonly IServicioAptitudes _servicioAptitudes;
        private readonly IServicioTalentos _servicioTalentos;
        private readonly IServicioVacantes _servicioVacantes;
        private readonly IServicioPerfiles _servicioPerfiles;

        public MuseFiltroController(IServicioMuseFiltro servicioFiltroMuse,
            IServicioAptitudes servicioAptitudes,
            IServicioVacantes servicioVacantes,
            IServicioTalentos servicioTalentos,
            IServicioPerfiles servicioPerfiles)
        {
            _servicioFiltroMuse = servicioFiltroMuse;
            _servicioAptitudes = servicioAptitudes;
            _servicioVacantes = servicioVacantes;
            _servicioTalentos = servicioTalentos;
            _servicioPerfiles = servicioPerfiles;
        }
        public ActionResult BuscadorMuse()
        {
            ViewBag.Aptitudes = ObtenerAptitudes();
            ViewBag.Talentos = ObtenerTalentos();
            ViewBag.Perfiles = ObtenerPerfiles();
            ViewBag.Vacantes = ObtenerVacantes();
            return View();
        }

        [HttpPost]
        public PartialViewResult BuscadorMuse(MuseFiltroInputModel entrada)
        {
            switch ((TipoFiltroMuse)entrada.Filtro)
            {
                case TipoFiltroMuse.Aptitudes:
                    var tpv = _servicioFiltroMuse.ObtenerUsandoAptitudes(entrada);
                    return PartialView("_partialAptitudesResultado", tpv);
                case TipoFiltroMuse.Talento:
                    var apv = _servicioFiltroMuse.ObtenerUsandoTalento(entrada);
                    return PartialView("_partialTalentoResultado", apv);
                case TipoFiltroMuse.Perfiles:
                    var tp = _servicioFiltroMuse.ObtenerUsandoPerfiles(entrada);
                    return PartialView("_partialPerfilesResultado", tp);
                case TipoFiltroMuse.Vacante:
                    var tv = _servicioFiltroMuse.ObtenerUsandoVacante(entrada);
                    return PartialView("_partialVacanteResultado", tv);

            }
            var resultado = _servicioFiltroMuse.ObtenerUsandoAptitudes(entrada);
            return PartialView("_partialPrueba");
        }

        #region CargadoresData
        public List<SelectListItem> ObtenerAptitudes()
        {
            var aptitudes = _servicioAptitudes.ObtenerAptitudes();
            return aptitudes.Select(a => new SelectListItem()
            {
                Text = a.Nombre,
                Value = a.AptitudID.ToString()
            }).ToList();
        }

        public List<SelectListItem> ObtenerTalentos()
        {
            var talentos = _servicioTalentos.ObtenerTalentos();
            return talentos.Select(t => new SelectListItem()
            {
                Text = t.NombreSimple,
                Value = t.TalentoID.ToString()
            }).ToList();
        }

        public List<SelectListItem> ObtenerPerfiles()
        {
            var perfiles = _servicioPerfiles.ObtenerPerfiles();
            return perfiles.Select(p => new SelectListItem()
            {
                Text = p.Nombre,
                Value = p.PerfilID.ToString()
            }).ToList();
        }

        public List<SelectListItem> ObtenerVacantes()
        {
            var vacantes = _servicioVacantes.ObtenerVacantes();
            return vacantes.Select(v => new SelectListItem()
            {
                Text = v.Nombre,
                Value = v.VacanteID.ToString()
            }).ToList();
        }
        #endregion
    }
}