﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TalentosApp.Web.Startup))]
namespace TalentosApp.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            System.AppDomain.CurrentDomain.SetData("DataDirectory2", @"C:\dell");
            ConfigureAuth(app);
        }
    }
}
