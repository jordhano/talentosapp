﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contractos.AccesoDatos.Repositorios;
using TalentosApp.Nucleo.Entidades;

namespace TalentosApp.AccesoDatos.Repositorios
{
    class RepositorioPerfiles: RepositorioBase<Perfiles>,IRepositorioPerfiles
    {
        private DbContext _contexto;
        public RepositorioPerfiles(DbContext contexto): base(contexto)
        {
            _contexto = contexto;
        }
        public IEnumerable<TEntity> ObtenerPerfilesDisponiblesVacante<TEntity>(int vacanteID) where TEntity : class
        {
            var consulta = @"
                            SELECT * FROM Perfiles
                            WHERE EstadoReg =1 AND PerfilID NOT IN (
	                            SELECT PerfilID FROM VacantesPerfiles
	                            WHERE EstadoReg = 1 AND VacanteID = @vacanteID
                            );";

            var resultado = _contexto.Database.SqlQuery<TEntity>(consulta, new SqlParameter()
            {
                DbType = System.Data.DbType.Int32,
                Value = vacanteID,
                ParameterName = "@vacanteID"
            }).ToList();
            return resultado;
        }
    }
}
