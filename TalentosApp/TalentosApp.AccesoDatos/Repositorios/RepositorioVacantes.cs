﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contractos.AccesoDatos.Repositorios;
using TalentosApp.Nucleo.Entidades;

namespace TalentosApp.AccesoDatos.Repositorios
{
    class RepositorioVacantes: RepositorioBase<Vacantes>, IRepositorioVacantes
    {
        private readonly DbContext _contexto;
        public RepositorioVacantes(DbContext contexto): base(contexto)
        {
            _contexto = contexto;
        }

        public IEnumerable<TEntity> ObtenerAptitudesVacante<TEntity>(int vacante) where TEntity : class
        {
            string consulta = @"
                                WITH AptitudesPerfiles AS (
	                                SELECT p.PerfilID, pa.AptitudID,NivelManejo,
		                                CASE UnidadTiempoExperiencia 
			                                WHEN 2 THEN (TiempoExperiencia*12)
			                                ELSE TiempoExperiencia
		                                END [TiempoExperiencia],p.Nombre
	                                FROM PerfilesAptitudes pa
	                                INNER JOIN Perfiles p
	                                ON pa.PerfilID = p.PerfilID
	                                WHERE pa.EstadoReg = 1
                                ), AptitudesVacanteSinPerfiles AS (
	                                SELECT VacanteID, AptitudID,NivelManejo,
		                                CASE UnidadTiempoExperiencia 
			                                WHEN 2 THEN (TiempoExperiencia*12)
			                                ELSE TiempoExperiencia
		                                END [TiempoExperiencia]
	                                FROM VacantesAptitudes va 
	                                WHERE va.EstadoReg = 1
                                ), AptitudesVacantesEnPerfiles AS (
	                                SELECT VacanteID, AptitudID, ap.NivelManejo, ap.TiempoExperiencia
	                                FROM VacantesPerfiles va
	                                INNER JOIN AptitudesPerfiles ap
	                                ON va.PerfilID = ap.PerfilID
	                                WHERE va.EstadoReg = 1 
                                ), AptitudesVacantes AS (
	                                SELECT va.*, vc.Nombre FROM (
		                                SELECT *,NULL[AptitudPerfilID] FROM AptitudesVacanteSinPerfiles
		                                UNION
		                                SELECT *,1 [AptitudPerfilID] FROM AptitudesVacantesEnPerfiles
		                                WHERE AptitudID NOT IN (SELECT AptitudID FROM AptitudesVacanteSinPerfiles)
	                                )va 
	                                INNER JOIN Vacantes vc
	                                ON va.VacanteID = vc.VacanteID
                                ) 

                                SELECT av.VacanteID, av.AptitudID,a.Nombre [Aptitud],NivelManejo,TiempoExperiencia,AptitudPerfilID,
                                vp.VacantePriorizacionID,vp.ValoracionMinimaAptitud,vp.Orden
                                FROM AptitudesVacantes av
                                INNER JOIN Aptitudes a
                                ON av.AptitudID = a.AptitudID
                                LEFT JOIN (SELECT * FROM VacantePriorizacion vp WHERE vp.EstadoReg = 1 AND vp.VacanteID = @vacante) vp
                                ON (av.VacanteID = vp.VacanteID AND av.AptitudID = vp.AptitudID)
                                WHERE av.VacanteID = @vacante
                                ORDER BY vp.Orden
            ";
            var resultado = _contexto.Database.SqlQuery<TEntity>(consulta, new SqlParameter()
            {
                SqlDbType = System.Data.SqlDbType.Int,
                Value = vacante,
                ParameterName = "@vacante"
            }).ToList();
            return resultado;
        }


        public int ConsultarProximoNumeroOrden(int vacante)
        {
            string query = @"SELECT TOP 1 (CAST([Orden] AS INT)) ACTUAL
                            FROM VacantePriorizacion
                            WHERE VacanteID = @vacante
                            ORDER BY Orden DESC";

            var detalle = _contexto.Database.SqlQuery<int>(query, new SqlParameter() { 
                DbType = System.Data.DbType.Int32,
                Value = vacante,
                ParameterName = "@vacante"
            }).ToList().FirstOrDefault();

            return detalle;
        }
    }
}
