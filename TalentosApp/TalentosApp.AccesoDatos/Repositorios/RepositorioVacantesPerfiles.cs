﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contratos.AccesoDatos.Repositorios;
using TalentosApp.Nucleo.Entidades;

namespace TalentosApp.AccesoDatos.Repositorios
{
    class RepositorioVacantesPerfiles : RepositorioBase<VacantesPerfiles>, IRepositorioVacantesPerfiles
    {
        private readonly DbContext _contexto;
        public RepositorioVacantesPerfiles(DbContext contexto): base(contexto)
        {
            _contexto = contexto;
        }
    }
}
