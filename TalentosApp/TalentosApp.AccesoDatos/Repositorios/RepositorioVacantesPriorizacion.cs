﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contratos.AccesoDatos.Repositorios;
using TalentosApp.Nucleo.Entidades;

namespace TalentosApp.AccesoDatos.Repositorios
{
    class RepositorioVacantesPriorizacion : RepositorioBase<VacantePriorizacion>, IRepositorioVacantesPriorizacion
    {
        private readonly DbContext _contexto;
        public RepositorioVacantesPriorizacion(DbContext contexto): base(contexto)
        {
            _contexto = contexto;
        }
    }
}
