﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contratos.AccesoDatos.Repositorios;
using TalentosApp.Nucleo.Entidades;

namespace TalentosApp.AccesoDatos.Repositorios
{
    class RepositorioTalentosAptitudes: RepositorioBase<TalentosAptitudes>,IRepositorioTalentosAptitudes
    {
        private readonly DbContext _contexto;
        public RepositorioTalentosAptitudes(DbContext contexto): base(contexto)
        {
            _contexto = contexto;
        }
    }
}
