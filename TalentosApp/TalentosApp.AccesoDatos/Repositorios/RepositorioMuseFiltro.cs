﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contractos.AccesoDatos.Repositorios;


namespace TalentosApp.AccesoDatos.Repositorios
{
    class RepositorioMuseFiltro : IRepositorioMuseFiltro
    {
        public readonly DbContext _context;
        public readonly string _consulta;
        public readonly string _consultaInicial;
        public readonly string _consultaInicialV2;
        public readonly string _consultaFinal;
        public RepositorioMuseFiltro(DbContext context)
        {
            _context = context;
            _consultaInicial = @"SELECT *, SUM(Estado) over (partition by TipoEntidad,EntityID) [ResultadoEval] FROM (";
            _consultaInicialV2 = @"SELECT *, SUM(Estado) over (partition by TipoEntidad,EntityID) [ResultadoEval] FROM (";
            _consultaFinal = @") a ORDER BY [ResultadoEval] desc";
            _consulta = @"
                            WITH AptitudesTalentos AS (
	                            SELECT TalentoAptitudID,ta.TalentoID,AptitudID,NivelManejo,
		                            CASE UnidadTiempoExperiencia 
			                            WHEN 2 THEN (TiempoExperiencia*12)
			                            ELSE TiempoExperiencia
		                            END [TiempoExperiencia], t.NombreSimple [Nombre] 
	                            FROM TalentosAptitudes ta
	                            INNER JOIN Talentos t
	                            ON ta.TalentoID = t.TalentoID
	                            WHERE ta.EstadoReg = 1
                            ), AptitudesPerfiles AS (
	                            SELECT p.PerfilID, pa.AptitudID,NivelManejo,
		                            CASE UnidadTiempoExperiencia 
			                            WHEN 2 THEN (TiempoExperiencia*12)
			                            ELSE TiempoExperiencia
		                            END [TiempoExperiencia],p.Nombre
	                            FROM PerfilesAptitudes pa
	                            INNER JOIN Perfiles p
	                            ON pa.PerfilID = p.PerfilID
	                            WHERE pa.EstadoReg = 1
                            ),AptitudesVacanteSinPerfiles AS (
	                            SELECT VacanteID, AptitudID,NivelManejo,
		                            CASE UnidadTiempoExperiencia 
			                            WHEN 2 THEN (TiempoExperiencia*12)
			                            ELSE TiempoExperiencia
		                            END [TiempoExperiencia]
	                            FROM VacantesAptitudes va 
	                            WHERE va.EstadoReg = 1
                            ), AptitudesVacantesEnPerfiles AS (
	                            SELECT VacanteID, AptitudID, ap.NivelManejo, ap.TiempoExperiencia
	                            FROM VacantesPerfiles va
	                            INNER JOIN AptitudesPerfiles ap
	                            ON va.PerfilID = ap.PerfilID
	                            WHERE va.EstadoReg = 1 
                            ), AptitudesVacantes AS (
	                            SELECT va.*, vc.Nombre FROM (
		                            SELECT * FROM AptitudesVacanteSinPerfiles
		                            UNION
		                            SELECT * FROM AptitudesVacantesEnPerfiles
		                            WHERE AptitudID NOT IN (SELECT AptitudID FROM AptitudesVacanteSinPerfiles)
	                            )va 
	                            INNER JOIN Vacantes vc
	                            ON va.VacanteID = vc.VacanteID
                            ) 
                            ";
        }

        public IEnumerable<T> FiltroPorAptitudes<T>(int[] aptitudes) where T : class
        {
            if (aptitudes == null || !aptitudes.Any())
                return Enumerable.Empty<T>();

            var parametros = new List<SqlParameter>();
            var parametrosStr = new StringBuilder();
            for (int i = 0; i < aptitudes.Length; i++)
            {
                parametrosStr.Append("@parametro" + i);
                parametros.Add(new SqlParameter()
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = aptitudes[i],
                    ParameterName = "@parametro" + i
                });

                if ((i + 1) < aptitudes.Length)
                    parametrosStr.Append(",");

            }


            var consulta = new StringBuilder(_consulta);
            consulta.Append(_consultaInicial);
            consulta.Append(string.Format(@"
                                            SELECT apt.Nombre[Aptitud],Entidades.*, CASE WHEN apt.AptitudID IN ({0}) THEN 4 ELSE 0 END  [Estado]
                                            FROM (
	                                            SELECT 1 [TipoEntidad],TalentoID [EntityID],AptitudID,NivelManejo,TiempoExperiencia,Nombre
	                                            FROM AptitudesTalentos at
	                                            WHERE  at.TalentoID IN 
	                                            (
		                                            SELECT DISTINCT at.TalentoID
		                                            FROM AptitudesTalentos at
		                                            WHERE (at.AptitudID IN ({0}))
	                                            )
	                                            UNION ALL

	                                            SELECT 2 [TipoEntidad],PerfilID [EntityID],AptitudID,NivelManejo,TiempoExperiencia,Nombre
	                                            FROM AptitudesPerfiles ap
	                                            WHERE  ap.PerfilID IN 
	                                            (
		                                            SELECT DISTINCT ap.PerfilID
		                                            FROM AptitudesPerfiles ap
		                                            WHERE (ap.AptitudID IN ({0}))
	                                            )
	                                            UNION ALL

	                                            SELECT 3 [TipoEntidad],VacanteID [EntityID],AptitudID,NivelManejo,TiempoExperiencia,Nombre
	                                            FROM AptitudesVacantes av
	                                            WHERE  av.VacanteID IN 
	                                            (
		                                            SELECT DISTINCT av.VacanteID
		                                            FROM AptitudesVacantes av
		                                            WHERE (av.AptitudID IN ({0}))
	                                            )
                                            ) Entidades
                                            INNER JOIN Aptitudes apt
                                            ON Entidades.AptitudID = apt.AptitudID
                                            ", parametrosStr.ToString()));
            consulta.Append(_consultaFinal);

            return _context.Database.SqlQuery<T>(consulta.ToString(), parametros.ToArray()).ToList();
        }

        public IEnumerable<T> FiltroPorAptitudesVacante<T>(int vacante) where T : class
        {
            if (vacante < 1)
                return Enumerable.Empty<T>();


            var consulta = new StringBuilder();
            consulta.Append(@"
                            WITH AptitudesTalentos AS (
	                            SELECT TalentoAptitudID,ta.TalentoID,AptitudID,NivelManejo,
		                            CASE UnidadTiempoExperiencia 
			                            WHEN 2 THEN (TiempoExperiencia*12)
			                            ELSE TiempoExperiencia
		                            END [TiempoExperiencia], t.NombreSimple [Nombre] 
	                            FROM TalentosAptitudes ta
	                            INNER JOIN Talentos t
	                            ON ta.TalentoID = t.TalentoID
	                            WHERE ta.EstadoReg = 1
                            ),AptitudesPerfiles AS (
	                            SELECT p.PerfilID, pa.AptitudID,NivelManejo,
		                            CASE UnidadTiempoExperiencia 
			                            WHEN 2 THEN (TiempoExperiencia*12)
			                            ELSE TiempoExperiencia
		                            END [TiempoExperiencia],p.Nombre
	                            FROM PerfilesAptitudes pa
	                            INNER JOIN Perfiles p
	                            ON pa.PerfilID = p.PerfilID
	                            WHERE pa.EstadoReg = 1
                            ), AptitudesVacanteSinPerfiles AS (
	                            SELECT VacanteID, AptitudID,NivelManejo,
		                            CASE UnidadTiempoExperiencia 
			                            WHEN 2 THEN (TiempoExperiencia*12)
			                            ELSE TiempoExperiencia
		                            END [TiempoExperiencia]
	                            FROM VacantesAptitudes va 
	                            WHERE va.EstadoReg = 1
                            ), AptitudesVacantesEnPerfiles AS (
	                            SELECT VacanteID, AptitudID, ap.NivelManejo, ap.TiempoExperiencia
	                            FROM VacantesPerfiles va
	                            INNER JOIN AptitudesPerfiles ap
	                            ON va.PerfilID = ap.PerfilID
	                            WHERE va.EstadoReg = 1 
                            ), AptitudesVacantes AS (
	                            SELECT va.*, vc.Nombre FROM (
		                            SELECT *,NULL[AptitudPerfilID] FROM AptitudesVacanteSinPerfiles
		                            UNION
		                            SELECT *,1 [AptitudPerfilID] FROM AptitudesVacantesEnPerfiles
		                            WHERE AptitudID NOT IN (SELECT AptitudID FROM AptitudesVacanteSinPerfiles)
	                            )va 
	                            INNER JOIN Vacantes vc
	                            ON va.VacanteID = vc.VacanteID
                            ), AptitudesVacante AS (
	                            SELECT av.VacanteID, av.AptitudID,a.Nombre [Aptitud],NivelManejo,TiempoExperiencia,AptitudPerfilID,
	                            vp.VacantePriorizacionID,vp.ValoracionMinimaAptitud,vp.Orden
	                            FROM AptitudesVacantes av
	                            INNER JOIN Aptitudes a
	                            ON av.AptitudID = a.AptitudID
	                            LEFT JOIN (SELECT * FROM VacantePriorizacion vp WHERE vp.EstadoReg = 1 AND vp.VacanteID = @vacante) vp
	                            ON (av.VacanteID = vp.VacanteID AND av.AptitudID = vp.AptitudID)
	                            WHERE av.VacanteID = @vacante
                            )");
            consulta.Append(@"
                SELECT apt.Nombre[Aptitud],Entidades.*,av.Orden,
                    CASE WHEN av.AptitudID IS NOT NULL
                    THEN
	                    CASE 
		                    WHEN Entidades.TiempoExperiencia >= av.TiempoExperiencia AND Entidades.NivelManejo >= av.NivelManejo THEN
			                    4
		                    WHEN Entidades.NivelManejo >= av.NivelManejo THEN
			                    3
		                    WHEN Entidades.TiempoExperiencia >= av.TiempoExperiencia THEN
			                    2
		                    ELSE
			                    1
	                    END
                    ELSE 
	                    0 
                    END  [Estado]
                FROM (
	                SELECT 1 [TipoEntidad],TalentoID [EntityID],AptitudID,NivelManejo,TiempoExperiencia,Nombre
	                FROM AptitudesTalentos at
	                WHERE  at.TalentoID IN 
	                (
		                SELECT DISTINCT at.TalentoID
		                FROM AptitudesTalentos at
		                WHERE (at.AptitudID IN (select AptitudID from AptitudesVacante))
	                )
                ) Entidades
                INNER JOIN Aptitudes apt
                ON Entidades.AptitudID = apt.AptitudID
                LEFT JOIN AptitudesVacante av 
                ON Entidades.AptitudID = av.AptitudID
            ");
            
            return _context.Database.SqlQuery<T>(consulta.ToString(), new SqlParameter()
            {
                ParameterName = "@vacante",
                DbType = System.Data.DbType.Int32,
                Value = vacante
            }).ToList();
        }

        public IEnumerable<T> FiltroPorAptitudesPerfil<T>(int[] perfil) where T : class
        {
            if (perfil== null)
                return Enumerable.Empty<T>();

            var parametros = new List<SqlParameter>();
            var parametrosStr = new StringBuilder();
            for (int i = 0; i < perfil.Length; i++)
            {
                parametrosStr.Append("@parametro" + i);
                parametros.Add(new SqlParameter()
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = perfil[i],
                    ParameterName = "@parametro" + i
                });

                if ((i + 1) < perfil.Length)
                    parametrosStr.Append(",");

            }

            var consulta = new StringBuilder(_consulta);
            consulta.Append(string.Format(@"
                            ,AptitudesPerfil AS (
	                            SELECT * FROM AptitudesPerfiles
                                WHERE PerfilID IN ({0})
                            )", parametrosStr.ToString()));
            consulta.Append(_consultaInicialV2);
            consulta.Append(@"
                            SELECT apt.Nombre[Aptitud],Entidades.*,
                            CASE WHEN av.AptitudID IS NOT NULL
		                    THEN
			                    CASE 
				                    WHEN Entidades.TiempoExperiencia >= av.TiempoExperiencia AND Entidades.NivelManejo >= av.NivelManejo THEN
					                    4
				                    WHEN Entidades.NivelManejo >= av.NivelManejo THEN
					                    3
				                    WHEN Entidades.TiempoExperiencia >= av.TiempoExperiencia THEN
					                    2
				                    ELSE
					                    1
			                    END
		                    ELSE 
			                    0 
		                    END  [Estado]
                            FROM (
	                            SELECT 1 [TipoEntidad],TalentoID [EntityID],AptitudID,NivelManejo,TiempoExperiencia,Nombre
	                            FROM AptitudesTalentos at
	                            WHERE  at.TalentoID IN 
	                            (
		                            SELECT DISTINCT at.TalentoID
		                            FROM AptitudesTalentos at
		                            WHERE (at.AptitudID IN (select AptitudID from AptitudesPerfil))
	                            )
                            ) Entidades
                            INNER JOIN Aptitudes apt
                            ON Entidades.AptitudID = apt.AptitudID
	                        LEFT JOIN AptitudesPerfil av 
	                        ON Entidades.AptitudID = av.AptitudID
                            ");
            consulta.Append(_consultaFinal);

            return _context.Database.SqlQuery<T>(consulta.ToString(), parametros.ToArray()).ToList();
        }

        public IEnumerable<T> FiltroPorAptitudesTalento<T>(int talento) where T : class
        {
            if (talento < 1)
                return Enumerable.Empty<T>();


            var consulta = new StringBuilder(_consulta);
            consulta.Append(@"
                            ,AptitudesTalento AS (
	                            SELECT * FROM AptitudesTalentos
	                            WHERE TalentoID = @talento
                            )");
            consulta.Append(_consultaInicialV2);
            consulta.Append(@"
                            SELECT apt.Nombre[Aptitud],Entidades.*,
                            CASE WHEN av.AptitudID IS NOT NULL
		                    THEN
			                    CASE 
				                    WHEN av.TiempoExperiencia >= Entidades.TiempoExperiencia AND av.NivelManejo >=  Entidades.NivelManejo THEN
					                    4
				                    WHEN av.NivelManejo >= Entidades.NivelManejo THEN
					                    3
				                    WHEN av.TiempoExperiencia >= Entidades.TiempoExperiencia THEN
					                    2
				                    ELSE
					                    1
			                    END
		                    ELSE 
			                    0 
		                    END  [Estado]
                            FROM (
	                            SELECT 2 [TipoEntidad],PerfilID [EntityID],AptitudID,NivelManejo,TiempoExperiencia,Nombre
	                            FROM AptitudesPerfiles ap
	                            WHERE  ap.PerfilID IN 
	                            (
		                            SELECT DISTINCT ap.PerfilID
		                            FROM AptitudesPerfiles ap
		                            WHERE (ap.AptitudID IN (select AptitudID from AptitudesTalento))
	                            )
	                            UNION ALL

	                            SELECT 3 [TipoEntidad],VacanteID [EntityID],AptitudID,NivelManejo,TiempoExperiencia,Nombre
	                            FROM AptitudesVacantes av
	                            WHERE  av.VacanteID IN 
	                            (
		                            SELECT DISTINCT av.VacanteID
		                            FROM AptitudesVacantes av
		                            WHERE (av.AptitudID IN (select AptitudID from AptitudesTalento))
	                            )
                            ) Entidades
                            INNER JOIN Aptitudes apt
                            ON Entidades.AptitudID = apt.AptitudID
	                        LEFT JOIN AptitudesTalento av 
	                        ON Entidades.AptitudID = av.AptitudID
                            ");
            consulta.Append(_consultaFinal);
            return _context.Database.SqlQuery<T>(consulta.ToString(), new SqlParameter()
            {
                ParameterName = "@talento",
                DbType = System.Data.DbType.Int32,
                Value = talento
            }).ToList();
        }
    }
}
