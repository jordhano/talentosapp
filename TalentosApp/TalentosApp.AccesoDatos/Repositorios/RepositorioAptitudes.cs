﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contractos.AccesoDatos.Repositorios;
using TalentosApp.Nucleo.Entidades;

namespace TalentosApp.AccesoDatos.Repositorios
{
    class RepositorioAptitudes:RepositorioBase<Aptitudes>, IRepositorioAptitudes
    {
        private readonly DbContext _contexto;
        public RepositorioAptitudes(DbContext contexto):base(contexto)
        {
            _contexto = contexto;
        }

        public IEnumerable<TEntity> ObtenerAptitudesDisponiblesPerfil<TEntity>(int perfil) where TEntity : class
        {
            var consulta = @"
                                SELECT * FROM Aptitudes
                                WHERE EstadoReg =1 AND AptitudID NOT IN (
	                                SELECT AptitudID FROM PerfilesAptitudes
	                                WHERE EstadoReg = 1 AND PerfilID = @perfil
                                );
                            ";
            var resultado = _contexto.Database.SqlQuery<TEntity>(consulta, 
                                                new SqlParameter()
                                                {
                                                    ParameterName = "@perfil",
                                                    SqlDbType = System.Data.SqlDbType.Int,
                                                    Value = perfil
                                                }).ToList();

            return resultado;

        }

        public IEnumerable<TEntity> ObtenerAptitudesDisponiblesTalento<TEntity>(int talento) where TEntity : class
        {
            var consulta = @"
                                SELECT * FROM Aptitudes
                                WHERE EstadoReg =1 AND AptitudID NOT IN (
	                                SELECT AptitudID FROM TalentosAptitudes
	                                WHERE EstadoReg = 1 AND TalentoID = @talento
                                );
                            ";
            var resultado = _contexto.Database.SqlQuery<TEntity>(consulta,
                                                new SqlParameter()
                                                {
                                                    ParameterName = "@talento",
                                                    SqlDbType = System.Data.SqlDbType.Int,
                                                    Value = talento
                                                }).ToList();

            return resultado;

        }


        public IEnumerable<TEntity> ObtenerAptitudesDisponiblesVacante<TEntity>(int vacanteID) where TEntity : class
        {
            var consulta = @"
                                SELECT * FROM Aptitudes
                                WHERE EstadoReg =1 AND AptitudID NOT IN (
	                                SELECT AptitudID FROM VacantesAptitudes
	                                WHERE EstadoReg = 1 AND VacanteID = @Vacante
                                );
                            ";
            var resultado = _contexto.Database.SqlQuery<TEntity>(consulta,
                                                new SqlParameter()
                                                {
                                                    ParameterName = "@Vacante",
                                                    SqlDbType = System.Data.SqlDbType.Int,
                                                    Value = vacanteID
                                                }).ToList();

            return resultado;
        }
    }
}
