﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contractos.AccesoDatos.Repositorios;

namespace TalentosApp.AccesoDatos.Repositorios
{
    public abstract class RepositorioBase<TEntity>: IRepositorioBase<TEntity> where TEntity:class
    {
        private readonly DbContext _contexto;
        public RepositorioBase(DbContext contexto)
        {
            _contexto = contexto;
        }

        public IEnumerable<TEntity> ObtenerRegistros(System.Linq.Expressions.Expression<Func<TEntity, bool>> filter)
        {
            return _contexto.Set<TEntity>().Where(filter);
        }

        public TEntity ObtenerRegistro(System.Linq.Expressions.Expression<Func<TEntity, bool>> filter)
        {
            return _contexto.Set<TEntity>().SingleOrDefault(filter);
        }

        public TEntity Insertar(TEntity entidad)
        {
            _contexto.Set<TEntity>().Add(entidad);
            return entidad;
        }

        public int CantidadRegistros()
        {
            return _contexto.Set<TEntity>().Count();
        }

        public int CantidadRegistros(System.Linq.Expressions.Expression<Func<TEntity, bool>> filter)
        {
            return _contexto.Set<TEntity>().Count(filter);
        }

        public void CrearRange(IEnumerable<TEntity> entities)
        {
            _contexto.Set<TEntity>().AddRange(entities);
        }
    }
}
