﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contratos.AccesoDatos.Repositorios;
using TalentosApp.Nucleo.Entidades;

namespace TalentosApp.AccesoDatos.Repositorios
{
    class RepositorioTalentos : RepositorioBase<Talentos>,IRepositorioTalentos
    {
        private readonly DbContext _contexto;
        public RepositorioTalentos(DbContext contexto): base(contexto)
        {
            _contexto = contexto;
        }
    }
}
