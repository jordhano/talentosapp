﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contratos.AccesoDatos.Repositorios;
using TalentosApp.Nucleo.Entidades;

namespace TalentosApp.AccesoDatos.Repositorios
{
    class RepositorioVacantesAptitudes : RepositorioBase<VacantesAptitudes>, IRepositorioVacantesAptitudes
    {
        private readonly DbContext _contexto;
        public RepositorioVacantesAptitudes(DbContext contexto): base(contexto)
        {
            _contexto = contexto;
        }
    }
}
