﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.AccesoDatos.Modelos;
using TalentosApp.AccesoDatos.Repositorios;
using TalentosApp.Contractos.AccesoDatos.Repositorios;
using TalentosApp.Contratos.AccesoDatos;
using TalentosApp.Contratos.AccesoDatos.Repositorios;

namespace TalentosApp.AccesoDatos
{
    public class UnidadTrabajo: IUnidadTrabajo
    {
        public TalentosEntities _contexto { get; set; }
        public UnidadTrabajo(TalentosEntities contexto)
        {
            _contexto = contexto;
        }
        public bool GuardarCambios()
        {
            try
            {
                _contexto.SaveChanges();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                var resultado = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    resultado.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                       resultado.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage));
                    }
                    
                }
                Console.WriteLine(resultado.ToString());
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        //public IRepositorioBase<TEntity> RepositorioBase<TEntity>() where TEntity : class, new()
        //{
        //    return new Repositorios.RepositorioBase<TEntity>(_contexto);
        //}


        public IRepositorioAptitudes RepositorioAptitudes()
        {
            return new Repositorios.RepositorioAptitudes(_contexto);
        }


        public IRepositorioVacantes RepositorioVacantes()
        {
            return new Repositorios.RepositorioVacantes(_contexto);
        }


        public IRepositorioPerfiles RepositorioPerfiles()
        {
            return new Repositorios.RepositorioPerfiles(_contexto);
        }


        public IRepositorioMuseFiltro RepositorioMuseFiltro()
        {
            return new RepositorioMuseFiltro(_contexto);
        }

        public IRepositorioClientes RepositorioClientes()
        {
            return new RepositorioClientes(_contexto);
        }

        public IRepositorioPerfilesAptitudes RepositorioPerfilesAptitudes()
        {
            return new RepositorioPerfilesAptitudes(_contexto);
        }

        public IRepositorioTalentos RepositorioTalentos()
        {
            return new RepositorioTalentos(_contexto);
        }

        public IRepositorioTalentosAptitudes RepositorioTalentosAptitudes()
        {
            return new RepositorioTalentosAptitudes(_contexto);
        }

        public IRepositorioVacantesAptitudes RepositorioVacantesAptitudes()
        {
            return new RepositorioVacantesAptitudes(_contexto);
        }

        public IRepositorioVacantesPerfiles RepositorioVacantesPerfiles()
        {
            return new RepositorioVacantesPerfiles(_contexto);
        }

        public IRepositorioVacantesPriorizacion RepositorioVacantesPriorizacion()
        {
            return new RepositorioVacantesPriorizacion(_contexto);
        }
    }
}
