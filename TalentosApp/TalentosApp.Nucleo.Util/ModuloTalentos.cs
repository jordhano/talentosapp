﻿using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Conventions;
using TalentosApp.Servicios.Base;
using TalentosApp.AccesoDatos;
using TalentosApp.AccesoDatos.Modelos;
using TalentosApp.Contratos.AccesoDatos;

namespace TalentosApp.Nucleo.Util
{
    public class ModuloTalentos : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IUnidadTrabajo>().ToConstructor(o => new UnidadTrabajo(new TalentosEntities()));
            var baseClass = new Type[] { typeof(ServicioAptitudes) };
            this.Bind(o => o.FromAssemblyContaining(baseClass).SelectAllClasses().BindAllInterfaces());
        }
    }
}
