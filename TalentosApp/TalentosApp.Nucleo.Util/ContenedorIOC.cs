﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.Nucleo.Util
{
    public static class ContenedorIOC
    {
        private static IKernel _kernel;
        public static IKernel ObtenerContenedor()
        {
            if (_kernel == null)
                _kernel = new StandardKernel();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            _kernel.Load(new ModuloTalentos());

            return _kernel;
        }

        public static TObject ObtenerInstancia<TObject>()
        {
            var obj = _kernel.Get<TObject>();

            return obj;
        }

        public static void AgregarBinding<TBinding, TImplementation>()
            where TImplementation : TBinding
        {
            _kernel.Bind<TBinding>().To<TImplementation>();
        }
    }

}
