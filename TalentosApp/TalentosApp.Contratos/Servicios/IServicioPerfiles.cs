﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.TransferenciaDatos.VistaModelos;

namespace TalentosApp.Contractos.Servicios
{
    public interface IServicioPerfiles
    {
        int RegistrarPerfil(PerfilInputModel entrada);
        int AgregarAptitudPerfil(PerfilAptitudInputModel entrada);
        PerfilViewModel ObtenerPerfil(int perfilID);
        PerfilAptitudViewModel ObtenerPerfilAptitud(int perfilAptitudID);
        int ModificarAptitudPerfil(PerfilAptitudInputModel entrada);
        int RemoverAptitudPerfil(int aptitudPerfilID);
        IEnumerable<PerfilViewModel> ObtenerPerfiles();

        IEnumerable<PerfilViewModel> ObtenerPerfilesDisponibleVacantes(int vacanteID);

        int ModificarPerfil(PerfilInputModel entrada);

        int RemoverPerfil(int perfilID);
    }
}
