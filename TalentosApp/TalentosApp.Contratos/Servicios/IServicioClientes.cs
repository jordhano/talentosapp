﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.TransferenciaDatos.VistaModelos;

namespace TalentosApp.Contractos.Servicios
{
    public interface IServicioClientes
    {
        IEnumerable<ClientesVw> ObtenerClientes();
        int RegistrarCliente(ClienteInputModel entrada);
        ClientesVw ObtenerCliente(int id);
        int ModificarCliente(ClienteInputModel entrada);
        int RemoverCliente(int clienteID);
    }
}
