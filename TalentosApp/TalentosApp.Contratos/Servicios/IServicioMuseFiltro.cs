﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.TransferenciaDatos.VistaModelos.MuseFiltro;

namespace TalentosApp.Contractos.Servicios
{
    public interface IServicioMuseFiltro
    {
        AptitudesMuseResultadoVw ObtenerUsandoAptitudes(MuseFiltroInputModel entrada);
        TalentoMuseResultadoVw ObtenerUsandoTalento(MuseFiltroInputModel entrada);
        PerfilesMuseResultadoVw ObtenerUsandoPerfiles(MuseFiltroInputModel entrada);
        VacanteMuseResultadoVw ObtenerUsandoVacante(MuseFiltroInputModel entrada);
    }
}
