﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.TransferenciaDatos.VistaModelos;

namespace TalentosApp.Contractos.Servicios
{
    public interface IServicioTalentos
    {
        IEnumerable<TalentosVw> ObtenerTalentos();
        int RegistrarTalentos(TalentoInputModel input);
        TalentosVw ObtenerTalento(int TalentoID);
        int ModificarTalentos(TalentoInputModel input);
        int EliminarTalento(int TalentoID);

        TalentoAptitudViewModel ObtenerTalentoAptitud(int talentoAptitudID);
        int AgregarTalentoAptitud(TalentoAptitudInputModel entrada);
        int ModificarTalentoAptitud(TalentoAptitudInputModel entrada);
        int RemoverTalentoApitutd(int talentoAptitudID);
    }
}
