﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.TransferenciaDatos.VistaModelos;


namespace TalentosApp.Contractos.Servicios
{
    public interface IServicioVacantes
    {
        #region CRUD-Basico
        IEnumerable<VacantesVw> ObtenerVacantes();
        VacantesVw ObtenerVacante(int id);
        int RegistrarVacante(VacanteInputModel entrada, out string mensaje);
        int ModificarVacante(VacanteInputModel entrada, out string mensaje);
        int RemoverVacante(int vacanteId, out string mensaje);
        int CancelarVacante(int vacanteId, out string mensaje);
        #endregion

        #region CRUD-VacanteAptitud
        VacanteAptitudViewModel ObtenerVacanteAptitud(int vacanteAptitudID);
        int AgregarVacanteAptitud(VacanteAptitudInputModel entrada, out string mensaje);
        int ModificarVacanteAptitud(VacanteAptitudInputModel entrada, out string mensaje);
        int RemoverVacanteAptitud(int vacanteAptitudID, out string mensaje);
        #endregion

        #region CRUD-VacantePerfil
        VacantePerfilViewModel ObtenerVacantePerfil(int vacantePerfilID);
        int AgregarVacantePerfil(VacantePerfilInputModel entrada, out string mensaje);
        int ModificarVacantePerfil(VacantePerfilInputModel entrada, out string mensaje);
        int RemoverVacantePerfil(int vacantePerfilID, out string mensaje);
        #endregion

        #region Priorizacion
        IEnumerable<VacantePriorizacionViewModel> ObtenerAptitudesPriorizacion(int vacante);
        #endregion


        int AgregarPriorizacion(VacantePriorizacionInputModel entrada);

        int RemoverPriorizacion(int priorizacionID);

        VacantePriorizacionViewModel ObtenerAptitudPriorizacion(int priorizacionID);

        int ModificarPriorizacion(int priorizacionID, int valoracionAptitud);

        int AlterarOrden(int vacanteID, int[] ordenAplicado);
    }
}
