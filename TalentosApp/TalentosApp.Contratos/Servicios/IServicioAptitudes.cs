﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.TransferenciaDatos.Entradas;
using TalentosApp.TransferenciaDatos.VistaModelos;

namespace TalentosApp.Contractos.Servicios
{
    public interface IServicioAptitudes
    {
        IEnumerable<AptitudesVw> ObtenerAptitudes();
        int RegistrarAptitud(AptitudInputModel entrada);

        IEnumerable<AptitudesVw> ObtenerAptitudesDisponiblePerfil(int perfilID);
        IEnumerable<AptitudesVw> ObtenerAptitudesDisponibleTalentos(int talentoID);
        IEnumerable<AptitudesVw> ObtenerAptitudesDisponibleVacantes(int vacanteID);

        AptitudesVw ObtenerAptitud(int id);

        int ModificarAptitud(AptitudInputModel entrada);

        int RemoverAptitud(int aptitudId);

        
    }
}
