﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contractos.AccesoDatos.Repositorios;
using TalentosApp.Contratos.AccesoDatos;
using TalentosApp.Contratos.AccesoDatos.Repositorios;

namespace TalentosApp.Contratos.AccesoDatos
{
    public interface IUnidadTrabajo
    {
        bool GuardarCambios();

        //IRepositorioBase<TEntity> RepositorioBase<TEntity>() where TEntity : class,new();
        IRepositorioAptitudes RepositorioAptitudes();
        IRepositorioClientes RepositorioClientes();
        IRepositorioMuseFiltro RepositorioMuseFiltro();
        IRepositorioPerfiles RepositorioPerfiles();
        IRepositorioPerfilesAptitudes RepositorioPerfilesAptitudes();
        IRepositorioTalentos RepositorioTalentos();
        IRepositorioTalentosAptitudes RepositorioTalentosAptitudes();
        IRepositorioVacantes RepositorioVacantes();
        IRepositorioVacantesAptitudes RepositorioVacantesAptitudes();
        IRepositorioVacantesPerfiles RepositorioVacantesPerfiles();
        IRepositorioVacantesPriorizacion RepositorioVacantesPriorizacion();
    }
}
