﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.Contractos.AccesoDatos.Repositorios
{
    public interface IRepositorioBase<TEntity> where TEntity:class
    {
        IEnumerable<TEntity> ObtenerRegistros(Expression<Func<TEntity, bool>> filter);
        TEntity ObtenerRegistro(Expression<Func<TEntity, bool>> filter);
        TEntity Insertar(TEntity entidad);

        int CantidadRegistros();
        int CantidadRegistros(Expression<Func<TEntity, bool>> filter);

        void CrearRange(IEnumerable<TEntity> entities);
    }
}
