﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Nucleo.Entidades;

namespace TalentosApp.Contractos.AccesoDatos.Repositorios
{
    public interface IRepositorioPerfiles : IRepositorioBase<Perfiles>
    {
        IEnumerable<TEntity> ObtenerPerfilesDisponiblesVacante<TEntity>(int vacanteID) where TEntity : class;
    }
}
