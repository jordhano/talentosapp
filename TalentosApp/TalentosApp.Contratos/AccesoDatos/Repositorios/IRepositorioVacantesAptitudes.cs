﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Contractos.AccesoDatos.Repositorios;
using TalentosApp.Nucleo.Entidades;

namespace TalentosApp.Contratos.AccesoDatos.Repositorios
{
    public interface IRepositorioVacantesAptitudes : IRepositorioBase<VacantesAptitudes>
    {
    }
}
