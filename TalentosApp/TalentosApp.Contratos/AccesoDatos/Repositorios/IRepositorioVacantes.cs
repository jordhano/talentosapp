﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Nucleo.Entidades;

namespace TalentosApp.Contractos.AccesoDatos.Repositorios
{
    public interface IRepositorioVacantes: IRepositorioBase<Vacantes>
    {
        IEnumerable<TEntity> ObtenerAptitudesVacante<TEntity>(int vacante) where TEntity : class;

        int ConsultarProximoNumeroOrden(int vacante);
    }
}
