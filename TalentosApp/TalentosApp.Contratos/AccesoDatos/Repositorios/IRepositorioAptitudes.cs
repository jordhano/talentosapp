﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Nucleo.Entidades;

namespace TalentosApp.Contractos.AccesoDatos.Repositorios
{
    public interface IRepositorioAptitudes : IRepositorioBase<Aptitudes>
    {
        IEnumerable<TEntity> ObtenerAptitudesDisponiblesPerfil<TEntity>(int perfil) where TEntity:class;
        IEnumerable<TEntity> ObtenerAptitudesDisponiblesTalento<TEntity>(int talento) where TEntity : class;
        IEnumerable<TEntity> ObtenerAptitudesDisponiblesVacante<TEntity>(int vacanteID) where TEntity : class;
    }
}
