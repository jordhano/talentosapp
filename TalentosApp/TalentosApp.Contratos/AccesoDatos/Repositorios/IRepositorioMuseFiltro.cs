﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.Contractos.AccesoDatos.Repositorios
{
    public interface IRepositorioMuseFiltro
    {
        IEnumerable<T> FiltroPorAptitudes<T>(int[] aptitudes) where T : class;
        IEnumerable<T> FiltroPorAptitudesVacante<T>(int vacante) where T : class;
        IEnumerable<T> FiltroPorAptitudesPerfil<T>(int[] perfil) where T : class;
        IEnumerable<T> FiltroPorAptitudesTalento<T>(int talento) where T : class;
    }
}
