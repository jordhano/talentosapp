﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.Entradas
{
    public class ClienteInputModel
    {
        public int ClienteID { get; set; }
        public string Nombre { get; set; }
    }
}
