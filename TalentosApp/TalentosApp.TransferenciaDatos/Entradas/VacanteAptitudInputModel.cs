﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.Entradas
{
    public class VacanteAptitudInputModel
    {
        public int VacanteAptitudID { get; set; }
        public int VacanteID { get; set; }
        public int AptitudID { get; set; }
        public int NivelManejo { get; set; }
        public int TiempoExperiencia { get; set; }
        public int UnidadTiempoExperiencia { get; set; }
    }
}
