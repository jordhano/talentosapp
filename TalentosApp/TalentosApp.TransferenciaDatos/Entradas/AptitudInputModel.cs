﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.Entradas
{
    public class AptitudInputModel
    {
        public int AptitudID { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int TipoAptitud { get; set; }
    }
}
