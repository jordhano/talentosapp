﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.Entradas
{
    public class VacantePriorizacionInputModel
    {
        public int VacanteID { get; set; }
        public int ValoracionAptitud { get; set; }
        public int AptitudID { get; set; }
        public int? AptitudPerfilID { get; set; }
    }
}
