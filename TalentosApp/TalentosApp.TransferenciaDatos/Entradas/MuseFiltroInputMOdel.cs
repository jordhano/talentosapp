﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.Entradas
{
    public class MuseFiltroInputModel
    {
        public int Filtro { get; set; }
        public int[] Aptitudes { get; set; }
        public int? Talento { get; set; }
        public int[] Perfiles { get; set; }
        public int? Vacante { get; set; }
    }
}
