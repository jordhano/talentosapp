﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.Entradas
{
    public class VacantePerfilInputModel
    {
        public int VacantePerfilID { get; set; }
        public int VacanteID { get; set; }
        public int PerfilID { get; set; }
    }
}
