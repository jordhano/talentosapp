﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos
{
    public class VacantePerfilViewModel
    {
        public int VacantePerfilID { get; set; }
        public int VacanteID { get; set; }
        public string Vacante { get; set; }
        public int PerfilID { get; set; }
        public string Perfil { get; set; }
    }
}
