﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos
{
    public class PerfilViewModel
    {
        public int PerfilID { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public IEnumerable<PerfilAptitudViewModel> AptitudesPerfil { get; set; }
    }
}
