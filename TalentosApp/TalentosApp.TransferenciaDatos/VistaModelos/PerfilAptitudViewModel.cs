﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos
{
    public class PerfilAptitudViewModel
    {
        public int PerfilAptitudID { get; set; }
        public string Aptitud { get; set; }
        public int AptitudID { get; set; }
        public int PerfilID { get; set; }
        public string Perfil { get; set; }
        public int NivelManejo { get; set; }
        public int TiempoExperiencia { get; set; }
        public int UnidadTiempoExperiencia { get; set; }
    }
}
