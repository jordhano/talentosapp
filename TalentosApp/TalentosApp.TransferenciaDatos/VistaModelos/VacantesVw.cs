﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos
{
    public class VacantesVw
    {
        public int VacanteID { get; set; }
        public int ClienteID { get; set; }
        public string Cliente { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int Estado { get; set; }

        public IEnumerable<VacanteAptitudViewModel> Aptitudes { get; set; }
        public IEnumerable<VacantePerfilViewModel> Perfiles { get; set; }
    }
}
