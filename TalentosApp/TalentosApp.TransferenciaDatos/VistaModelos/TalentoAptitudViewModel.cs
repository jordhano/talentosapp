﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos
{
    public class TalentoAptitudViewModel
    {
        public int TalentoAptitudID { get; set; }
        public int TalentoID { get; set; }
        public string Talento { get; set; }
        public int AptitudID { get; set; }
        public string Aptitud { get; set; }
        public int NivelManejo { get; set; }
        public int TiempoExperiencia { get; set; }
        public int UnidadTiempoExperiencia { get; set; }
        public int ValoracionAptitud { get; set; }
    }
}
