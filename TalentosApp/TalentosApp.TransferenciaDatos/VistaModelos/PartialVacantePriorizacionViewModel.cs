﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos
{
    public class PartialVacantePriorizacionViewModel
    {
        public IEnumerable<VacantePriorizacionViewModel> Especificadas { get; set; }
        public IEnumerable<VacantePriorizacionViewModel> NoEspecificadas { get; set; }
        public int VacanteID { get; set; }

        public PartialVacantePriorizacionViewModel(IEnumerable<VacantePriorizacionViewModel> aptitudes,int vacante)
        {
            Especificadas = aptitudes.Where(a => a.VacantePriorizacionID.HasValue == true).OrderBy(v => v.Orden).ToList();
            NoEspecificadas = aptitudes.Where(a => a.VacantePriorizacionID.HasValue == false).ToList();
            VacanteID = vacante;
        }
    }
    
}
