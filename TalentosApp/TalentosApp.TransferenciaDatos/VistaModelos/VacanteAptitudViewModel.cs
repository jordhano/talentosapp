﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos
{
    public class VacanteAptitudViewModel
    {
        public int VacanteAptitudID { get; set; }
        public int VacanteID { get; set; }
        public string Vacante { get; set; }
        public int AptitudID { get; set; }
        public string Aptitud { get; set; }
        public int NivelManejo { get; set; }
        public int TiempoExperiencia { get; set; }
        public int UnidadTiempoExperiencia { get; set; }
    }
}
