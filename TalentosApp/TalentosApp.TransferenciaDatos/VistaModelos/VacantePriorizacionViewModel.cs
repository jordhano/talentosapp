﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos
{
    public class VacantePriorizacionViewModel
    {
        public int VacanteID { get; set; }
        public int AptitudID { get; set; }
        public string Aptitud { get; set; }
        public int NivelManejo { get; set; }
        public int TiempoExperiencia { get; set; }
        public int? AptitudPerfilID { get; set; }
        public int? VacantePriorizacionID { get; set; }
        public int? ValoracionMinimaAptitud { get; set; }
        public int? Orden { get; set; }
    }
}
