﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos.MuseFiltro
{
    public class VacanteMuseResultadoVw
    {
        public int TotalFiltros { get; set; }
        public int TotalTalentos { get { return Talentos != null ? Talentos.Count() : 0; } }
        public int TotalAptitudes { get { return Aptitudes != null ? Aptitudes.Count() : 0; } }
        public IEnumerable<EntidadMuseResultadoVw> Talentos { get; set; }
        public IEnumerable<AptitudesVw> Aptitudes { get; set; }
    }
}
