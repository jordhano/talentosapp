﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos.MuseFiltro
{
    public class TuplaMuseResultadoVw
    {
        public int TipoEntidad { get; set; }
        public int EntityID { get; set; }
        public int AptitudID { get; set; }
        public int NivelManejo { get; set; }
        public int TiempoExperiencia { get; set; }
        public int Estado { get; set; }
        public int ResultadoEval { get; set; }
        public string Aptitud { get; set; }
        public string Nombre { get; set; }
        public int? Orden { get; set; }
    }
}
