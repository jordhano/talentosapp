﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos.MuseFiltro
{
    public class AptitudesMuseResultadoVw
    {
        public int TotalFiltros { get; set; }
        public int TotalTalentos { get { return Talentos != null ? Talentos.Count() : 0; } }
        public int TotalPerfiles { get { return Perfiles != null ? Perfiles.Count() : 0; } }
        public int TotalVacantes { get { return Vacantes != null ? Vacantes.Count() : 0; } }
        public IEnumerable<EntidadMuseResultadoVw> Talentos { get; set; }
        public IEnumerable<EntidadMuseResultadoVw> Perfiles { get; set; }
        public IEnumerable<EntidadMuseResultadoVw> Vacantes { get; set; }
    }
}
