﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos.MuseFiltro
{
    public class EntidadMuseResultadoVw
    {
        public string Nombre { get; set; }
        public List<AptitudMuseResultadoVw> Aplica { get; set; }
        public List<AptitudMuseResultadoVw> Manejo { get; set; }
        public List<AptitudMuseResultadoVw> Tiempo { get; set; }
        public List<AptitudMuseResultadoVw> Miscelaneos { get; set; }
        public List<AptitudMuseResultadoVw> NA { get; set; }
        public int TotalAplica { get { return Aplica != null ? Aplica.Count() : 0; } }
        public int TotalManejo { get { return Aplica != null ? Manejo.Count() : 0; } }
        public int TotalTiempo { get { return Aplica != null ? Tiempo.Count() : 0; } }
        public int TotalMiscelaneos { get { return Aplica != null ? Miscelaneos.Count() : 0; } }
        public int TotalNA { get { return Aplica != null ? NA.Count() : 0; } }
        public int Ranking { get; set; }

        public EntidadMuseResultadoVw()
        {
            Aplica = new List<AptitudMuseResultadoVw>();
            Manejo = new List<AptitudMuseResultadoVw>();
            Tiempo = new List<AptitudMuseResultadoVw>();
            Miscelaneos = new List<AptitudMuseResultadoVw>();
            NA = new List<AptitudMuseResultadoVw>();
        }
    }
}
