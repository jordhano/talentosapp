﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos.MuseFiltro
{
    public class AptitudMuseResultadoVw
    {
        public string Nombre { get; set; }
        public int Estado { get; set; }
    }
}
