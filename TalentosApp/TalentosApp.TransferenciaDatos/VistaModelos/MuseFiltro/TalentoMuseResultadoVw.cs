﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos.MuseFiltro
{
    public class TalentoMuseResultadoVw
    {
        public int TotalFiltros { get; set; }
        public int TotalPerfiles { get { return Perfiles != null ? Perfiles.Count() : 0; } }
        public int TotalVacantes { get { return Vacantes != null ? Vacantes.Count() : 0; } }
        public int TotalAptitudes { get { return Aptitudes != null ? Aptitudes.Count() : 0; } }
        public IEnumerable<EntidadMuseResultadoVw> Perfiles { get; set; }
        public IEnumerable<EntidadMuseResultadoVw> Vacantes { get; set; }
        public IEnumerable<AptitudesVw> Aptitudes { get; set; }
    }
}
