﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos
{
    public class ClientesVw
    {
        public int ClienteID { get; set; }
        public string Nombre { get; set; }
    }
}
