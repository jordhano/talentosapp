﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalentosApp.TransferenciaDatos.VistaModelos
{
    public class TalentosVw
    {
        public int TalentoID { get; set; }
        public string NombreSimple { get; set; }
        public string NombreCompleto { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public int Sexo { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Email { get; set; }
        public int TipoDocumentoIdentidad { get; set; }
        public string DocumentoIdentidad { get; set; }

        public IEnumerable<TalentoAptitudViewModel> Aptitudes { get; set; }
    }
}
