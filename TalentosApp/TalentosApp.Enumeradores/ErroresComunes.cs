﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Enumeradores.Exts;

namespace TalentosApp.Enumeradores
{
    public enum ErroresComunes
    {
        [Description("Error guardando en la base de datos")]
        ErrorGuardando =-1,
        [Description("Se ha colocado un campo vacio")]
        CampoVacio = -2,
        [Description("Se ha colocado un campo con un valor invalido")]
        CampoInvalidado = -3,
        [Description("Esta opción aun no ha sido desarrollada")]
        OperacionNoImplementada =-4,
        [Description("Existe valor repetido")]
        ValoresRepetido = -5,
        [Description("Valor no econtrado")]
        ValoresNoEncontrado = -6,
        [Description("Valores no valido")]
        ValoresNoValido = -7,
        [Description("Se esta intentando duplicar datos, operación invalida")]
        Duplicidad = -8

    }
}
