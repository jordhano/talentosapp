﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Enumeradores.Exts;

namespace TalentosApp.Enumeradores
{
    public enum TipoAptitud
    {
        Idiomatica = 1,
        Tecnica = 2,
        Subjetiva=3
    }
}
