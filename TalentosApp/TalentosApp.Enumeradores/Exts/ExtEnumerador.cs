﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace TalentosApp.Enumeradores.Exts
{
    public static class ExtEnumerador
    {
        public static string GetDescriptionString(this Enum @val)
        {
            try 
            {
                var attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString())
                    .GetCustomAttributes(typeof(DescriptionAttribute), false);
                return attributes.Length > 0 ? attributes[0].Description : val.ToString();

            }
            catch (Exception)
            {
                return "N/A";
            }
        }
    }
}
