﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Enumeradores.Exts;

namespace TalentosApp.Enumeradores
{
    enum ValoracionAptitud
    {
        MuyMal = 5,
        Mal = 4,
        Normal = 3,
        Buena = 2,
        MuyBuena =1,
        NoValorizado =0
    }
}
