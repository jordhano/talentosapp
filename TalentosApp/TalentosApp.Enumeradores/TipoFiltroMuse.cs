﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Enumeradores.Exts;

namespace TalentosApp.Enumeradores
{
    public enum TipoFiltroMuse
    {
        Aptitudes = 1,
        Talento = 2,
        Perfiles = 3,
        Vacante = 4
    }
}
