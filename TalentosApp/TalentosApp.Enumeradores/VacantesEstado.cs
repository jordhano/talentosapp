﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Enumeradores.Exts;

namespace TalentosApp.Enumeradores
{
    public enum VacantesEstado
    {
        Notificada = 1,
        Proceso =2, 
        Concluida =3,
        Cancelada = 4,
    }
}
