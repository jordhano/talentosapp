﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Enumeradores.Exts;

namespace TalentosApp.Enumeradores
{
    public enum TipoEntidadFiltroMuse
    {
        Talento = 1,
        Perfil = 2,
        Vacante = 3
    }
}
