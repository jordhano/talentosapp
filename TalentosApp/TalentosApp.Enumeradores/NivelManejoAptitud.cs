﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Enumeradores.Exts;

namespace TalentosApp.Enumeradores
{
    public enum NivelManejoAptitud
    {
        Elemental = 1,
        Intermedio = 2,
        Experto = 3
    }
}
