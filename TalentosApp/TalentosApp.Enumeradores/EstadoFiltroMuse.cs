﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalentosApp.Enumeradores.Exts;

namespace TalentosApp.Enumeradores
{
    public enum EstadoFiltroMuse
    {
        Cumple = 4,
        Maneja = 3,
        Tiempo = 2,
        Misc = 1,
        NoCumple = 0
    }
}
